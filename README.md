# ionic

Comandos ionic CLI

ionic start [myApp] [template] --v2 --ts

ionic start [myApp] [template] <blank,tabs(default), sidemenu, maps ...>

cd [myApp]

ionic serve => levantar servidor 

ionic login

ionic upload => Suibir appp https://apps.ionic.io

* Add a platform (ios or Android): ionic platform add ios [android]

ionic package build android => cosntruir aplicacion
ionic package download 
ionic package list =>[id]

 * Build your app: ionic build <PLATFORM>

 * Simulate your app: ionic emulate <PLATFORM>

 * Run your app on a device: ionic run <PLATFORM>

 * Package an app using Ionic package service: ionic package <MODE> <PLATFORM>

For more help use ionic --help or ionic docs

Generar paginas  
ionic g page myPage
√ Create app/pages/my-page/my-page.html
√ Create app/pages/my-page/my-page.js
√ Create app/pages/my-page/my-page.scss

ionic generate --list 
Available generators:
 * component
 * directive
 * page
 * pipe
 * provider
 * tabs
