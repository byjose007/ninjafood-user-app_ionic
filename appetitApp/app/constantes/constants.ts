export class Constants {
    SERVER_IP: string;
    /**
     * header para configuraciones de envio de SMS
     */
    headers = {
        'authorization': "Basic Q2lkc2VjdXJlOlNNU2NkczQ1Ng==",
        'content_type': "application/json",
        'accept': "application/json"
    }
    constructor() {

        //this.SERVER_IP = 'http://localhost:8000';
        //this.SERVER_IP = 'http://172.18.250.48:9000';
        //this.SERVER_IP = 'http://172.18.250.138:8000';
        //this.SERVER_IP = 'https://www.ajamar.com';
        this.SERVER_IP = 'https://goodappetit.com';
    }
    
}