import {Component} from '@angular/core';
import {NavController, NavParams, AlertController} from 'ionic-angular';
import { SocialSharing } from 'ionic-native';
import {CategoriaService} from '../../providers/categoria-service/categoria-service';
import {MenuPage} from '../menu/menu';
import {CarritoPage} from '../carrito/carrito';
import {PlatoModel} from '../../providers/models/plato-model';
import {Constants} from '../../constantes/constants';
@Component({
    templateUrl: 'build/pages/categorias/categorias.html',
    //directives: [CarritoPage],
    providers: [CategoriaService, Constants]

})
export class CategoriasPage {
    menu:string = 'menu';
    categoriaSucursal: any[];
    categorias: any;
    id_categorias: any;
    id_menu: any;
    id_usuario:any;
    datosSucursal: any[];
    lista_categorias: any[];
    lista_productos: PlatoModel[] = [];
    menus: any[] = [];
    SERVER_IP: string;
    constructor(private navCtrl: NavController, private CategoriaService: CategoriaService, private NavParams: NavParams, private alertCtrl: AlertController, constants: Constants) {
        this.SERVER_IP = constants.SERVER_IP;
        this.lista_categorias = [];
        this.menus = this.NavParams.get('menus');
         this.id_usuario = this.NavParams.get('id_usuario');
        this.datosSucursal = this.NavParams.get('sucursal');
        this.datosSucursal = this.updateMainPath(this.datosSucursal);
        this.loadCategoriasSucursal();
        if (this.NavParams.get('lista_productos') != undefined)
            this.lista_productos = this.NavParams.get('lista_productos');
    }

    onPageBack(){
        if (this.lista_productos.length > 0) {
            let prompt = this.alertCtrl.create({
                title: 'Login',
                message: "¿Deseas salir del restaurante? al salir perderas tu carrito.",               
                buttons: [
                    {
                        text: 'Si',
                        handler: data => {
                            console.log('Cancel clicked');
                            this.navCtrl.pop();
                        }
                    },
                    {
                        text: 'No',
                        handler: data => {
                            //console.log('Saved clicked');
                             //this.navCtrl.remove(1);
                        }
                    }
                ]
            });
            prompt.present();
            return false;

        }else{
             this.navCtrl.pop();
        }
       
    }

    updateMainPath(datosSucursal) {
        var aux;
        if (datosSucursal != null && typeof datosSucursal !== undefined){
            aux = datosSucursal.id_proveedor.id_categoria.ruta_foto != undefined ? datosSucursal.id_proveedor.id_categoria.ruta_foto.replace('http://127.0.0.1:8080', this.SERVER_IP) : "";
            datosSucursal.id_proveedor.id_categoria.ruta_foto = aux;
        }
        return datosSucursal;
    }

    private loadCategoriasSucursal() {
        let categorias = [];
        let id_categorias = [];
        for (let menu in this.menus)
        {
            if(this.menus[menu].id_sucursal == this.datosSucursal['id_sucursal'])
            {
                this.id_menu = this.menus[menu].id_menu;
            }
        }
        this.CategoriaService.getCategoriasSucursal(this.id_menu)
            .then(data => {
                this.categoriaSucursal = data;
                for (let cate in this.categoriaSucursal) {
                    id_categorias.push(this.categoriaSucursal[cate].id_platocategoria.id_platocategoria);
                    categorias.push(this.categoriaSucursal[cate].id_platocategoria.categoria)
                }

                this.categorias = categorias.filter(function (categoria, i) {
                    return categorias.indexOf(categoria) == i;
                })

                this.id_categorias = id_categorias.filter(function (id_categoria, i) {
                    return id_categorias.indexOf(id_categoria) == i;
                })

                for (let i in this.categorias) {
                    this.lista_categorias.push({
                        id: this.id_categorias[i],
                        categoria: this.categorias[i]
                    });
                }
            });

    }

    public gotoMenu(id_categoria) {
        this.navCtrl.push(MenuPage, {id_usuario:this.id_usuario, id_menu:this.id_menu, id_categoria, lista_productos: this.lista_productos,sucursal:this.datosSucursal });
    }


    public verCarrito(): void {
        //this.navCtrl.pop();
        this.navCtrl.push(CarritoPage, { lista_productos: this.lista_productos }
        );
        //this.navCtrl.pop();
    }



    // Compartir restaurante en redes sociales
    public facebookShare() {
        SocialSharing.shareViaFacebook("Message via Twitter", null, null)
            .then(() => {
                alert('SocialSharing ok');
            },
            () => {
                alert('SocialSharing failed');
            });


    }

}


