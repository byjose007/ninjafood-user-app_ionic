import {Component} from '@angular/core';
import {NavController, NavParams, ModalController, AlertController, LoadingController, Platform} from 'ionic-angular';
import {Facebook, GooglePlus, Network, SQLite, Device} from 'ionic-native';
import {ProveedorService} from '../../providers/proveedor-service/proveedor-service';
import {MenuPage} from '../menu/menu';
import {LoginPage} from '../login/login';
import {PerfilPage} from '../perfil/perfil';
import {validation} from '../validationCuent/validation';
import {CategoriasPage} from '../categorias/categorias';
import {PedidoPage} from '../pedidos/pedidos';
import {SugerenciaPage} from '../sugerencia/sugerencia';
import {AboutPage} from '../about/about';
import {TimerWrapper} from '@angular/core/src/facade/async';
import { CustomValidations } from '../../theme/CustomValidations';
import { AuthspaServices } from '../../providers/authspa/authspa';
import {Constants} from '../../constantes/constants';

@Component({
  templateUrl: 'build/pages/home/home.html',
  providers: [ProveedorService, AuthspaServices, Constants]
})
export class HomePage extends CustomValidations {
  sucursalesAbiertas: any[] = [];
  sucursalesCerradas: any[] = [];
  nuevos: any[];
  menus: any[] = [];
  categorias: any[] = [];
  usuario: any;
  buscador: boolean = false;
  private id: string;
  SERVER_IP: string;

  constructor(
    protected navCtrl: NavController,
    private params: NavParams,
    private alertCtrl: AlertController,
    public ProveedorService: ProveedorService,
    private authspaServices: AuthspaServices,
    private loadingCtrl: LoadingController,
    protected platform: Platform,
    public modalCtrl: ModalController,
    constants: Constants
  ) {    
    super(navCtrl, platform);
    this.id = Device.device.serial;
    this.usuario=JSON.parse(this.params.get('usuario'));
    console.log("Costructor Home");  
    console.log(JSON.stringify(this.usuario));
    this.SERVER_IP = constants.SERVER_IP; 
    this.updatePerfil(); 
    this.loadSucursales();
    this.loadCategorias();
    this.loadMenus();
    this.validatePermission();
    TimerWrapper.setInterval(() => {
      this.loadSucursales();
    }, 60000);
  }

  private loadMenus() {
    this.checkNetwork();
    this.ProveedorService.getMenus()
      .then(data => {
        this.menus = data;
      }).catch(error => {
        console.log(error);
      });
  }
  // Cargar restaurantes
  private loadSucursales() {
    // let loader = this.loadingCtrl.create({
    //   content: "Cargando..."
    // });
    // loader.present();
    this.checkNetwork();
    //this.sucursalesAbiertas = [];
    //this.sucursalesCerradas = [];
    this.ProveedorService.getSucursales()
      .then(data => {
        this.nuevos = data;
        let nuevo=false;
        for (let p in this.nuevos) {           
          if(this.nuevos[p].estado == 1)
          {
            let cambio=true;        
            for (let q in this.sucursalesAbiertas) {            
              if(this.sucursalesAbiertas[q].id_sucursal==this.nuevos[p].id_sucursal)
              {
                  cambio = false;
                  break;
              }              
            }
            if(cambio)
            {
              nuevo = true;
              break;
            }
          }
          else
          {
            let cambio=true;        
            for (let q in this.sucursalesCerradas) {            
              if(this.sucursalesCerradas[q].id_sucursal==this.nuevos[p].id_sucursal)
              {
                  cambio = false;
                  break;
              }              
            }
            if(cambio)
            {
              nuevo = true;
              break;
            }
          }          
        }
        if(nuevo || this.sucursalesAbiertas.length ==0)
        {
          this.sucursalesAbiertas = [];
          this.sucursalesCerradas = [];
          for (let sucursal in data) {
            if (data[sucursal].estado == 1) {
              data[sucursal].id_proveedor.id_perfil.ruta_foto = data[sucursal].id_proveedor.id_perfil.ruta_foto.replace('http://127.0.0.1:8080', this.SERVER_IP);
              this.sucursalesAbiertas.push(data[sucursal]);
            } else {
              data[sucursal].id_proveedor.id_perfil.ruta_foto = data[sucursal].id_proveedor.id_perfil.ruta_foto.replace('http://127.0.0.1:8080', this.SERVER_IP);
              this.sucursalesCerradas.push(data[sucursal]);
            }
          }
        }
        //   loader.dismiss();
      }).catch(error => {
        console.log(error);
        //   loader.dismiss();
      });
  }


  // Ir a la pagina de categorias
  public gotoCategorias(id_menu, sucursal, abierto) {
    this.cuentaVerificada();        
    if (abierto) {
      this.navCtrl.push(CategoriasPage, { menus:this.menus, sucursal, id_usuario:this.usuario.id_usuario });
    } else {
      this.presentAlert(sucursal.nombre_sucursal, sucursal.hora_atencion);
    }

  }

  // Alert cuando restaurante está cerrado
  private presentAlert(nombre, hora_atencion) {
    let alert = this.alertCtrl.create({
      title: nombre,
      subTitle: 'El restaurante ' + nombre + ' está cerrado, su horario de atención es: ' + hora_atencion,
      buttons: ['Muy bien, Gracias']
    });
    alert.present();
  }

  // buscador de restaurantes
  public getItems(ev: any) {
    let val = ev.target.value;
    if (val == " " || val == null || val == undefined || val == "") {
      val = "";
      this.sucursalesAbiertas = this.nuevos.filter((item) => {
        if (item.estado == 1)
          return (item.nombre_sucursal.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      this.sucursalesCerradas = this.nuevos.filter((item) => {
        if (item.estado != 1)
          return (item.nombre_sucursal.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    if (val && val.trim() != '') {
      this.sucursalesAbiertas = this.nuevos.filter((item) => {
        if (item.estado == 1)
          return (item.nombre_sucursal.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      this.sucursalesCerradas = this.nuevos.filter((item) => {
        if (item.estado != 1)
          return (item.nombre_sucursal.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

// Cargar categorias
  public loadCategorias() {
    this.ProveedorService.getCategorias()
      .then(data => {
        this.categorias = data;
      });
  }



  //filtros
  public filtrarCategoria(categoria) {
    let divhide = document.getElementsByClassName("main-cat");
    for (let  i=0; i < divhide.length;i++){
      var sd = <HTMLElement>divhide[i];
      sd.style.borderBottom = "none";
    }

    if (categoria == undefined) {
      this.loadSucursales();
      var div = document.getElementById("todos");
      div.style.borderBottom = "3px solid #E4022D";
    } else {
      this.sucursalesAbiertas = this.nuevos.filter((item) => {
        if (item.id_proveedor.id_categoria.categoria == categoria && item.estado == 1){
          var div = document.getElementById(item.id_proveedor.id_categoria.id_categoria);
          div.style.borderBottom = "3px solid #E4022D";

          return (item.id_proveedor.id_categoria.categoria.toLowerCase().indexOf(categoria.toLowerCase()) > -1);
        }
      })
      this.sucursalesCerradas = this.nuevos.filter((item) => {
        if (item.id_proveedor.id_categoria.categoria == categoria && item.estado != 1){
          var div = document.getElementById(item.id_proveedor.id_categoria.id_categoria);
          div.style.borderBottom = "3px solid #E4022D";
          
          return (item.id_proveedor.id_categoria.categoria.toLowerCase().indexOf(categoria.toLowerCase()) > -1);
        }
      })

    }

  }

  public salir() {
    Facebook.logout();
    GooglePlus.logout();
    this.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  logout() {
    let db = new SQLite();
    let sqlInsertSesion = 'UPDATE startApp set registro="0" WHERE token=?';
    db.openDatabase({
      name: 'goodappetit.db',
      location: 'default' // the location field is required
    }).then(rta => {
      db.executeSql(sqlInsertSesion, [this.id]).then(rta => {
        console.log("regsitrado");
      }, (err) => {
        console.error(JSON.stringify(err));
        console.error("Error: " + err);
      })
    });
  }

  //JY, Pull Down Recargar pantalla
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    setTimeout(() => {
      this.loadSucursales();
      this.loadCategorias();
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  public goToPerfil(){
    let modal = this.modalCtrl.create( PerfilPage,  {usuario: JSON.stringify(this.usuario)});    
    modal.present();
    modal.onWillDismiss(data =>{        
        this.usuario = JSON.parse(data.usuario);         
    })           
  }

  public goToValidation(){
    let modal = this.modalCtrl.create( validation,  {usuario: this.usuario});    
    modal.present();
    modal.onWillDismiss(data =>{        
        this.usuario.tel = data.usuario.tel;         
        this.usuario.verificado = data.usuario.verificado;
    });

    // this.navCtrl.push(validation, {usuario:this.usuario });
    console.log("enviando a la page validations (home)")
  }

  public goToPedidos(){
    this.navCtrl.push(PedidoPage, {id_usuario:this.usuario.id_usuario, reportar:false });

  }


  public goToSugerencias(){
    this.navCtrl.push(SugerenciaPage,{id_usuario:this.usuario.id_usuario});

  }

  public goToAbout(){
    this.navCtrl.push(AboutPage);
  }

  public showSearch(){
    if(this.buscador == false)
      this.buscador = true;
    else
      this.buscador = false;
  }

    ionViewWillEnter(){
      this.LocationValidator();
    }

    LocationValidator(){
      //Geolocation.
      if ((<any> window).cordova) {
        (<any> window).cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
          if(enabled == false){
            alert("Geolocalizacion Deshabilitada");
            //console.log("Antes presentAlertGeo");
            //this.presentAlertGeo("Geolocalizacion Deshabilitada", "Geolocalización");
            (<any> window).cordova.plugins.diagnostic.switchToLocationSettings();
          } 

        }, function(error) {
            //alert("The following error occurred: " + error);
            console.log("The following error occurred: " + error);
        });
      }
    }

    // Alert cuando restaurante está cerrado
  /*public presentAlertGeo(msg, nombre) {
    console.log("presentAlertGeo");
    let alert = this.alertCtrl.create({
      title: nombre,
      subTitle: msg,
      buttons: ['Ok']
    });
    alert.present();
  }*/
  validatePermission(){
    console.log("#######################this.id: "+this.id);
    if(this.id != undefined){
      (<any> window).cordova.plugins.diagnostic.requestRuntimePermission(function(status){
          switch(status){
              case (<any> window).cordova.plugins.diagnostic.runtimePermissionStatus.GRANTED:
                  console.log("Permission granted to use the camera");
                  break;
              case (<any> window).cordova.plugins.diagnostic.runtimePermissionStatus.NOT_REQUESTED:
                  console.log("Permission to use the camera has not been requested yet");
                  break;
              case (<any> window).cordova.plugins.diagnostic.runtimePermissionStatus.DENIED:
                  console.log("Permission denied to use the camera - ask again?");
                  break;
              case (<any> window).cordova.plugins.diagnostic.runtimePermissionStatus.DENIED_ALWAYS:
                  console.log("Permission permanently denied to use the camera - guess we won't be using it then!");
                  break;
          }
      }, function(error){
          console.error("The following error occurred: "+error);
      }, (<any> window).cordova.plugins.diagnostic.runtimePermission.ACCESS_FINE_LOCATION);
    }
  }
  
  cuentaVerificada(){
    var verif:any=this.usuario.verificado;
    if(!verif){
      var hedaer="Aviso";
      var body="Te recordamos que mientras no valides tu cuenta no podrás realizar compras."
      var button="Muy bien, Gracias";
      this.alertCuentaVerificada(hedaer, body, button);
    }
  }
  private alertCuentaVerificada(header:string, body:string, button:string) {
    let alert = this.alertCtrl.create({
      title: header,
      subTitle: body,
      buttons: [button]
    });
    alert.present();
  }

  updatePerfil(){
    console.log("updatePerfil");
    this.authspaServices.getPerfil(this.usuario.id_usuario)
      .then(data => {
        var user = data;
        /*this.usuario = {
          id_usuario: user[0].id_usuario.id,
          id_perfil: user[0].id_perfil,
          email: user[0].id_usuario.email,
          nombre: user[0].id_usuario.first_name,
          apellido: user[0].id_usuario.last_name,
          foto: user[0].ruta_foto,
          redsocial: "default",
          cedula: user[0].cedula,
          tel: user[0].telefono,
          verificado: user[0].cuentaVerif
        };*/
        this.usuario.tel = user[0].telefono;
        this.usuario.verificado = user[0].cuentaVerif;
        console.log("Actualizar Perfil Home ");
        this.updateLocalDB(this.usuario.id_usuario, this.usuario.id_perfil, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula, this.usuario.tel);
        console.log(JSON.stringify(this.usuario));
      }).catch(error => {
        console.log(error);
      });
  }

  public updateLocalDB(id_usuario:String, id_perfil:String, email:String, first_name:String, last_name:String, foto:String, 
  redsocial:String, cedula:String, telefono:String){
    console.log("####### LOGIN.TS #######");
    let db = new SQLite();
    let sqlInsertSesion = 'UPDATE startApp set registro="1", id_usuario="'+id_usuario+'", id_perfil="'+id_perfil+'", email="'+email+'", first_name="'+first_name
    +'", last_name="'+last_name+'", foto="'+foto+'", redsocial="'+redsocial+'", telefono="'+telefono+'" WHERE token=?';
    db.openDatabase({
      name: 'goodappetit.db',
      location: 'default' // the location field is required
    }).then(rta => {
      console.log("id: "+this.id);
      db.executeSql(sqlInsertSesion, [this.id]).then(rta=>{
        console.log("regsitrado");
      }, (err)=>{
        console.error(JSON.stringify(err));
        console.error("Error: "+err);
      })
    });
  }
}
