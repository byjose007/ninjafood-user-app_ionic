import {Component} from '@angular/core';
import {NavController, Platform, NavParams, AlertController, ActionSheetController, ViewController} from 'ionic-angular';
import {LicensesPage } from '../licenses/licenses';
import { Constants } from '../../constantes/constants';
import { AuthspaServices } from '../../providers/authspa/authspa';

@Component({
  templateUrl: 'build/pages/validationCuent/validation.html',
  providers: [AuthspaServices, Constants]
})
export class validation {
  user: any;
  userData: any;
  regularExp: any=/^(09\d)[ -]?(\d{3})[ -]?(\d{4})$/;
  exist: boolean;
  mensajeErrorNum = "";
  mensajeErrorCodeSend = "";
  vali: string = "conf";
  isAndroid: boolean = false;
  codigo: any[] = [];
  con: any;
  numeroTef:string;
  codeSend:number;
  codeRecive:number;
  verificado:boolean;
  botonInicial:boolean=true;
  
  constructor(
    private platform: Platform, 
    private navCtrl: NavController, 
    private constant: Constants, 
    private params: NavParams,
    private alertCtrl: AlertController,
    private authspaServices: AuthspaServices,
    private viewController: ViewController, 
    public actionsheetCtrl: ActionSheetController) {
      // this.user= this.params.get('usuario');
      this.user=this.params.get('usuario');
      console.log("datos de user recividos: --vv");
      console.log(JSON.stringify(this.user));
      this.verificado=this.user.verificado;
      this.isAndroid = platform.is('android');
      this.codigo=[{
        code: "+593",
        pais:"Ecuador"
        }
      ];
      this.con=constant.headers;
      console.log("despues del constructor");
      this.verificar();
  }
  /*public goToLicenses(){
    console.log("goToLicense: .I.");
    this.navCtrl.push(LicensesPage);
  }*/
  
  onPageBack(){
    if (this.userData==null) {
      // this.viewController.dismiss();
      this.navCtrl.pop();
    } else {
    this.viewController.dismiss({usuario: this.userData});
      
    }
  }
  /**
   * sendSMS
   */
  public sendSMS(code:number, numero:string):boolean {
    var res: boolean =false;

    var data = JSON.stringify({
    "from": "GoodAppetitMovil",
    "to": numero,
    "text": "Use "+code+" como codigo de verificacion de su cuenta GoodAppetit."
  });

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = false;

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === this.DONE) {
      console.log(this.responseText);
    }
  });

  xhr.open("POST", "http://api.infobip.com/sms/1/text/single");
  xhr.setRequestHeader("authorization", this.con.authorization);
  xhr.setRequestHeader("content-type", this.con.content_type);
  xhr.setRequestHeader("accept", this.con.accept);
  xhr.send(data);
  
  if (data!=null || data!="") {
    res=true;
    return res;
  } else{
    return res;
    }
  }
  validateNum(){
    var idUsuarioSend=parseInt(this.user.id_perfil);
    var data=this.numeroTef.match(this.regularExp)
    console.log(data);
    if (data==null) {
      this.mensajeErrorNum="Número Inválido";

    } else if (data.length>=4 || data[0].length>=10) {
      console.log(this.user);
      this.codeSend = Math.floor(Math.random() * (90000 - 9999)) + 9999;
      var numeroSend=this.numeroTef.substring(1,10);


      this.authspaServices.comprobarNumeroTelefono(idUsuarioSend, this.numeroTef)
      .then(data => {
        var user = data;        
        var resPerfil=user[0].id_perfil;        
        if (idUsuarioSend==resPerfil) {
          var re=this.sendSMS(this.codeSend, this.codigo[0].code+numeroSend);
          this.sendCodeEmail(this.user.email, this.codeSend); 
          if(re){
            this.vali="send";
            // this.actualizarUsuario(parseInt(this.user.id_perfil), '0000');  
          }             
        } else{
          this.mensajeErrorNum="El número ya está registrado.";
        }
        // this.viewController.dismiss({usuario: JSON.stringify(this.userData)});
      }).catch(error => {        
        console.log(error);
      });

    } 
    else {
      this.mensajeErrorNum="Numero invalido";
    }
  }


  private compararCodeSend(){
    if (this.codeSend==this.codeRecive) {
      this.vali="done";
      this.actualizarUsuario(parseInt(this.user.id_perfil), this.numeroTef);
      console.log("cuenta verificada, datos enviados: "+this.user.id_perfil+", tel:"+this.numeroTef+"");
      this.botonInicial=false;
    } else {
      this.mensajeErrorCodeSend="El código ingresado no es correcto.";
    }
  }

  
  verificar(){
    var tem:any=this.user.tel;
    // if (this.user.tel !=null ||  tem.length!=0) {
    if(this.verificado){
      if (tem.length==0 ) {
        this.exist=false;
      } else {
        this.numeroTef=this.user.tel;
        this.exist=true;
      }
    } else{
      if (tem==null || tem.length==0) {
        this.exist=false;
      } else {
        this.numeroTef=this.user.tel;
        this.exist=true;
      }
    }
    
    console.log("funcion verificar");
  }


openMenu() {
    let actionSheet = this.actionsheetCtrl.create({
      title: '¡Te recordamos, que si eliminar tu numero ya no podrás realizar compras!',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Eliminar número',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            this.actualizarUsuario(parseInt(this.user.id_perfil), '0000');
            this.exist=false;
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }


  actualizarUsuario(idPerfil:number, tel:string){       
    this.authspaServices.actualizarNumeroTelefono(idPerfil, tel)
      .then(data => {
        var user = data;        
        this.numeroTef=user[0].telefono;        
        this.userData = {
          /*id_usuario: user[0].id_usuario.id,
          //id_perfil(es): user[0].id_perfil,
          email: user[0].id_usuario.email,
          nombre: user[0].id_usuario.first_name,
          apellido: user[0].id_usuario.last_name,
          foto: user[0].ruta_foto,
          cedula: user[0].cedula,*/
          tel: user[0].telefono,
          verificado: user[0].cuentaVerif
        };   
        //this.userData.tel = user[0].telefono;
        //this.userData.verificado = user[0].cuentaVerif;
        // this.viewController.dismiss({usuario: JSON.stringify(this.userData)});
      }).catch(error => {        
        console.log(error);
      });
  }


  sendCodeEmail(correo:any, code:number){       
    this.authspaServices.sendCodeEmailMovil(correo, code)
      .then(data => {
        var user = data;        

      }).catch(error => {        
        console.log(error);
      });
  }

}