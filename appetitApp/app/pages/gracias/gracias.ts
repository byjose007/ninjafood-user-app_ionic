import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { PlatoModel } from '../../providers/models/plato-model';
import { formEntregaPage } from '../form-entrega/form-entrega';
import { HomePage } from '../home/home';
import { EstadoPage } from '../estado/estado';


/*
  Generated class for the CarritoPage page.
  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/gracias/gracias.html',
  //directives: [HomePage]
})
export class GraciasPage {
  lista_productos: any[];

  precio_envio: number = 1.50;
  total: number;
  tipo_envio: string;
  tipo_pago: string;
  comentario: string;
  sucursal: any;
  fecha:string;
  pedido:any;
  id_usuario:any;
  usuario:any;
  


  constructor(private navCtrl: NavController,
    private NavParams: NavParams,
    private alertCtrl: AlertController,
    protected platform: Platform) {
    this.total = 0;  
    this.lista_productos = this.NavParams.get('lista_productos');
    this.id_usuario = this.NavParams.get('id_usuario');
     this.pedido = this.NavParams.get('pedido');
    this.sucursal = this.NavParams.get('sucursal');
    this.tipo_pago = this.NavParams.get('tipo_pago');
    this.tipo_envio = this.NavParams.get('tipo_envio');
    this.total = this.NavParams.get('total');
    this.fecha = this.NavParams.get('fecha')+"";
    this.fecha = this.fecha.substring(4,25);
    
    this.initializeApp();
  }

  public verEstadoPedido() {
    this.navCtrl.push(EstadoPage, {pedido:this.pedido, tipo_envio: this.tipo_envio});
  }

  public gotoHome() {
    console.log('Back to HomePage :)');
    this.navCtrl.popToRoot();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      document.addEventListener('backbutton', () => {
      this.gotoHome();
    });
      document.addEventListener('backbutton', () => {
          console.log('Back button tapped :)');
          this.gotoHome();
      }, false);
    });
  }

}
