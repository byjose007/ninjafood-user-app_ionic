import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';


/*
  Generated class for the CarritoPage page.
  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/modalDireccion/modalDireccion.html'//,
  //directives: [IONIC_DIRECTIVES]
})
export class DireccionPage {
  lista_productos: any[];
  constructor(private navCtrl: NavController,
    private NavParams: NavParams,
    private alertCtrl: AlertController) {
    this.lista_productos = this.NavParams.get('lista_productos');
  }
}
