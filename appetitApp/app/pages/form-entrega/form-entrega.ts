import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { PlatoModel } from '../../providers/models/plato-model';
import { DireccionPage } from '../modalDireccion/modalDireccion';
import { GraciasPage } from '../gracias/gracias';
import { ProveedorService } from '../../providers/proveedor-service/proveedor-service';
import { Observable } from "rxjs/Rx";
import { TimerWrapper } from '@angular/core/src/facade/async';
import {UbicacionPage} from '../ubicacion/ubicacion';
/*
  Generated class for the CarritoPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/form-entrega/form-entrega.html',
  providers: [ProveedorService]
})
export class formEntregaPage {
  lista_productos: any[];
  intervalId: any;
  sub_total: number = 0;
  precio_envio: number = 0;
  total: number = 0;
  tipo_envio: string;
  tipo_pago: string;
  comentario: string;

  direcciones: any;
  id_direccion: any;
  id_usuario: any;
  id_perfil: any;

  num_direcciones: number;
  form_validado: boolean = false;

  num_casa: string;
  referencia: string;
  celular: number;
  calle_principal: string;
  calle_secundaria: string;

  pedido: any = {};
  nuevaDireccion: any = {};

  anio: number = new Date().getFullYear();
  mes: number = new Date().getMonth();
  dia: number = new Date().getDate();
  hora: number = new Date().getHours();
  minuto: number = new Date().getMinutes();
  segundo: number = new Date().getSeconds();
  myDate: String = new Date().toISOString();
  tiempoEstimado: number;
  date: Date;
  sucursal: any;
  lon: number;
  lat: number;
  isConfirmado: boolean = false;



  constructor(public navCtrl: NavController,
    public NavParams: NavParams, public proveedorService: ProveedorService,
    public alertCtrl: AlertController, public modalCtrl: ModalController) {
    this.date = new Date(this.anio, this.mes, this.dia, this.hora, this.minuto, this.segundo);

    this.total = 0;
    this.sub_total = 0;
    this.id_usuario = this.NavParams.get('id_usuario');
    this.lista_productos = this.NavParams.get('lista_productos');
    this.tipo_envio = this.NavParams.get('tipo_envio');
    this.tipo_pago = this.NavParams.get('tipo_pago');
    this.sucursal = this.NavParams.get('sucursal');
    this.comentario = this.NavParams.get('comentario');
    this.lista_productos.forEach(p => { this.sub_total += p.precio });
    this.precio_envio = this.NavParams.get('precio_envio');
    if(this.tipo_envio == 'recoger')
      this.total = (this.sub_total);
    else if (this.tipo_envio == 'domicilio')
      this.total = (this.sub_total + this.precio_envio);
    this.getDirecciones();
    this.intervalId = TimerWrapper.setInterval(() => {
      this.getDirecciones();
    }, 5000);
  }

  public crearNuevaDireccion() {

           /* this.navCtrl.push(UbicacionPage, {
              lon: this.lon, lat: this.lat, id_usuario: this.id_usuario,
              lista_productos: this.lista_productos, tipo_pago: this.tipo_pago,
              tipo_envio: this.tipo_envio, sucursal: this.sucursal
            });*/

    this.LocationValidator().then(rtaLV=>{
      console.log("rtaLV: "+rtaLV);
      if(rtaLV){
        this.validatePermission().then(rtaVP =>{
          console.log("rta: "+rtaVP);
          if(rtaVP){
            console.log("FROM-ENTREGA: (antes de llamar a la pagina ubicacion) Metodo: crear nueva direccion");
            console.log("lat: "+this.lat +" log: "+this.lon);
            this.navCtrl.push(UbicacionPage, {
              lon: this.lon, lat: this.lat, id_usuario: this.id_usuario,
              lista_productos: this.lista_productos, precio_envio: this.precio_envio, tipo_pago: this.tipo_pago,
              tipo_envio: this.tipo_envio, sucursal: this.sucursal, comentario: this.comentario
            });
          }
        }); 
      }
    });

    /*let confirm = this.alertCtrl.create({
      title: 'Nueva Dirección',
      message: 'Confirmar direccion de entrega',
      inputs: [
        {

          name: 'calle_principal',
          type: 'text',
          placeholder: 'Calle principal'
        },
        {
          name: 'calle_secundaria',
          type: 'text',
          placeholder: 'Calle secundaria'
        },
        {
          name: 'referencia',
          type: 'text',
          placeholder: 'Referencia'
        },
        {
          name: 'num_casa',
          type: 'text',
          placeholder: 'Número de casa'
        },
        {
          name: 'celular',
          type: 'number',
          placeholder: 'celular'
        }

      ],
      buttons: [
        {
          text: 'Guardar',
          handler: data => {

            this.validarForm(data.calle_principal, data.calle_secundaria,
              data.referencia, data.num_casa, data.celular, false);

          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            console.log('dirección cancelada');
          }
        }
      ]
    });
    confirm.present();*/
  }

  public mansajeAlert(titulo, texto) {
    let confirm = this.alertCtrl.create({
      title: titulo,
      message: texto
    });
    confirm.present();
    TimerWrapper.setTimeout(() => {
      confirm.dismiss();
    }, 2100);
  }


  public validarForm(calle_principal, calle_secundaria, referencia, num_casa, celular, firtsForm) {

    let celStr: string = celular.toString();
    let cel = celStr.substr(0, 2);

    if (calle_principal == "" || calle_secundaria == ""
      || referencia == "" || num_casa == "" || celular == "") {
      this.mansajeAlert("Error", "Todos los datos son obligatorios");
      return;
    } else if (cel != "09" && cel.length < 10) {
      this.mansajeAlert("Error", "Introduce un número de celular válido");
    } else {
      if (firtsForm) { // Si es la primera compra
        this.guardarDireccion(this.id_usuario, calle_principal, calle_secundaria, referencia, num_casa, celular, true);

      } else
        this.guardarDireccion(this.id_usuario, calle_principal, calle_secundaria, referencia, num_casa, celular, false);

    }

  }

  public validarDireccion(calle1, calle2, referencia, num_casa, celular, firtsDir) {

    //this.nuevaDireccion.calle_principal = calle1;


    this.nuevaDireccion = {
      "calle_principal": calle1,
      "calle_secundaria": calle2,
      "numero_casa": num_casa,
      "referencia": referencia,
      "latitud": 0,
      "longitud": 0,
      "id_perfil": this.id_perfil
    }

    this.proveedorService.guardarDireccion(this.nuevaDireccion).then(data => {
      if (firtsDir) {
        let dir: any = data;
        this.getDirecciones();
        this.id_direccion = dir.id_direccion;
        this.guardarPedido();

      } else {
        this.getDirecciones();
      }




      //data.forEach(p => p.fecha = p.fecha.substring(11,19) );
    });
  }


  public guardarDireccion(id_usuario, calle_principal, calle_secundaria, referencia, num_casa, celular, firtsDir) {

    this.proveedorService.getPerfil(id_usuario)
      .then(perfil => {
        let profile: any = perfil;
        this.id_perfil = profile[0].id_perfil;

        if (firtsDir)
          this.validarDireccion(calle_principal, calle_secundaria, referencia, num_casa, celular, true);
        else
          this.validarDireccion(calle_principal, calle_secundaria, referencia, num_casa, celular, false);
      });

  }

  public getDirecciones() {
    this.proveedorService.getDirecciones(this.id_usuario)
      .then(data => {

        this.direcciones = data;
        this.num_direcciones = this.direcciones.length;
        this.direcciones.reverse();
        if (this.num_direcciones > 0)
          this.id_direccion = this.direcciones[0].id_direccion;
        //data.forEach(p => p.fecha = p.fecha.substring(11,19) );
      });

  }
  public hacerPedido(tipo_envio, nuevo): void {
  this.isConfirmado = true;

    if (tipo_envio == 'domicilio' && nuevo) {
      if (!this.celular || (!this.id_direccion || !this.referencia || !this.num_casa)) {

        let alert = this.alertCtrl.create({
          title: "Mensaje",
          subTitle: "Todos los campos son obligatorios",
          buttons: ['OK']
        });
        alert.present();

      } else {
        this.guardarPedido();

      }
    } else {
      if (!this.celular && nuevo) {
        let alert = this.alertCtrl.create({
          title: "Mensaje",
          subTitle: "Introduce un número de celular válido",
          buttons: ['OK']
        });
        alert.present();
      } else {

        this.guardarPedido();


      }
    }
  }

  public guardarPedido() {

    if (this.tipo_envio == 'domicilio')
      this.pedido.id_tipo_pedido = 1;
    else{
      this.pedido.id_tipo_pedido = 2;
      if(!this.id_direccion)
        this.id_direccion = this.sucursal.id_direccion.id_direccion;
    }

    
    this.pedido.usuario = this.id_usuario;
    this.pedido.id_tipo_pago = this.obtenerIdTipoPago(this.tipo_pago);
    this.pedido.id_sucursal = this.sucursal.id_sucursal;
    this.pedido.id_estado = 1;
    this.pedido.id_direccion = this.id_direccion;
    this.pedido.fecha = this.date;
    console.log("antes de asignar a pedido.observaciones");
    console.log(this.comentario);
    this.pedido.observaciones = this.comentario;
    this.pedido.valor = this.total;
    this.pedido.tiempo = null;
    this.pedido.notificado = false;
    this.pedido.notificadoPersistente = "I";

    console.log('Guardar Pedido');
    console.log(this.pedido.observacion);

    this.proveedorService.guardarPedido(this.pedido, this.lista_productos).then(pedido => {
      this.cancelPeriodicIncrement();
      this.pedido = pedido;
      this.navCtrl.push(GraciasPage, {
        lista_productos: this.lista_productos, precio_envio: this.precio_envio,
        tipo_envio: this.tipo_envio, tipo_pago: this.tipo_pago,
        sucursal: this.sucursal, fecha: this.date, total: this.total, pedido: this.pedido, id_usuario: this.id_usuario
      });

    });

  }


  public obtenerIdTipoPago(tipo) {
    if (tipo == 'efectivo')
      return 1;
    else if (tipo == 'd_electronico')
      return 2;
    else
      return 3;
  }

  cancelPeriodicIncrement(): void {
    if (!!this.intervalId) {
      TimerWrapper.clearInterval(this.intervalId);
      this.intervalId = null;
    }
  };

  LocationValidator(){
    console.log("LocationValidator: Inicio");
    return new Promise(function (resolve, reject) {
      //var isEnable = false;
        if ((<any> window).cordova) {
          (<any> window).cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
            if(enabled == false){
              alert("Geolocalizacion Deshabilitada");
              //console.log("Antes presentAlertGeo");
              //this.presentAlertGeo("Geolocalizacion Deshabilitada", "Geolocalización");
              (<any> window).cordova.plugins.diagnostic.switchToLocationSettings();
            } 
            resolve(enabled);
          }, function(error) {
              //alert("The following error occurred: " + error);
              console.log("The following error occurred: " + error);
          });
    }
      });
    }

    validatePermission(){
      console.log("validatePermission: Inicio");
      return new Promise(function (resolve, reject) {
        (<any> window).cordova.plugins.diagnostic.requestRuntimePermission(function(status){
            switch(status){
                case (<any> window).cordova.plugins.diagnostic.runtimePermissionStatus.GRANTED:
                    console.log("Permission granted to use the camera");
                    resolve(true);
                    break;
                case (<any> window).cordova.plugins.diagnostic.runtimePermissionStatus.NOT_REQUESTED:
                    console.log("Permission to use the camera has not been requested yet");
                    resolve(false);
                    break;
                case (<any> window).cordova.plugins.diagnostic.runtimePermissionStatus.DENIED:
                    console.log("Permission denied to use the camera - ask again?");
                    resolve(false);
                    break;
                case (<any> window).cordova.plugins.diagnostic.runtimePermissionStatus.DENIED_ALWAYS:
                    console.log("Permission permanently denied to use the camera - guess we won't be using it then!");
                    resolve(false);
                    break;
            }
        }, function(error){
            resolve(false);
            console.error("The following error occurred: "+error);
      }, (<any> window).cordova.plugins.diagnostic.runtimePermission.ACCESS_FINE_LOCATION);
      });
  }

}


