import {Component} from '@angular/core';
import {NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import {PlatoService} from '../../providers/plato-service/plato-service';
import {PlatoDetallePage} from '../plato-detalle/plato-detalle';
import {PlatoModel} from '../../providers/models/plato-model';
import {CarritoPage} from '../carrito/carrito';
import {Constants} from '../../constantes/constants';

@Component({
    templateUrl: 'build/pages/menu/menu.html',
    providers: [PlatoService, Constants]//,
    //directives: [CarritoPage] 
})
export class MenuPage {
    sucursal: any;
    platos: any[];
    id_menu: any;
    id_usuario:any;
    id_categoria: string;
    lista_productos: any[];
    SERVER_IP: string;
   
    constructor(private navCtrl: NavController, private PlatoService: PlatoService,
        private NavParams: NavParams, private modalCtrl: ModalController, private viewCtrl: ViewController, 
        constants: Constants) {
        this.id_menu = this.NavParams.get('id_menu');
        this.id_usuario = this.NavParams.get('id_usuario');
        this.id_categoria = this.NavParams.get('id_categoria');
        this.lista_productos = this.NavParams.get('lista_productos');
        this.sucursal = this.NavParams.get('sucursal');
        this.SERVER_IP = constants.SERVER_IP;
        this.loadPLatos();
      
    }

    private presentModal(id_plato): void {
        //this.navCtrl.push(CarritoPage, {});

        /*let modal = this.modalCtrl.create(PlatoDetallePage, {
            id_plato, id_menu: this.id_menu, id_categoria: this.id_categoria,
            lista_productos: this.lista_productos
        });
        modal.onDidDismiss(data => {
            this.lista_productos = data;
        });      
        modal.present();
        //this.navCtrl.pop();*/

        this.navCtrl.push(PlatoDetallePage,{
            id_plato, id_menu: this.id_menu, id_categoria: this.id_categoria,sucursal:this.sucursal,
            lista_productos: this.lista_productos, id_usuario:this.id_usuario}
            );


    }


    private loadPLatos(): void {

        this.PlatoService.getPlatos(this.id_menu, this.id_categoria)
            .then(data => {
                this.platos = data;
                for (let plato in data) {  
                    data[plato].ruta_foto = data[plato].ruta_foto.replace('http://127.0.0.1:8080', this.SERVER_IP);
                }        
 
            });
    }


    /*public gotoDetallePlato(id_plato):void {
        this.navCtrl.push(PlatoDetallePage, {
            id_plato, id_menu: this.id_menu, id_categoria: this.id_categoria,
            lista_productos: this.lista_productos
        });
    }*/

    public verCarrito(): void {
        //this.navCtrl.pop();
        this.navCtrl.push(CarritoPage, { id_usuario:this.id_usuario, lista_productos: this.lista_productos,sucursal:this.sucursal }
        );
        //this.navCtrl.pop();
    }

}
