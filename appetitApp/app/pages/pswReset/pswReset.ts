import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {LicensesPage } from '../licenses/licenses';
import { CustomValidations } from '../../theme/CustomValidations';
import { AuthspaServices } from '../../providers/authspa/authspa';
import { AlertController } from 'ionic-angular';
import {LoginPage} from '../login/login';




@Component({
  templateUrl: 'build/pages/pswReset/pswReset.html',
  providers: [AuthspaServices]
})
export class PswReset {  
    email: string; 
    booleanEmail: boolean = false;    
    mensajes = "";


  constructor(
      private navCtrl: NavController,
      private authspaServices: AuthspaServices,
      public alertCtrl: AlertController      
      ) {
  }

  public envioPass(){
    console.log(this.email)

    this.authspaServices.comprobarEmailPsw(this.email).then(res => {
      console.log(res)
      
      if (res != 0) {        
        this.authspaServices.recuperarPass(this.email).then(data => {
          if (data == true) {
            let alert = this.alertCtrl.create({
              title: 'Recuperar Contraseña',
              message: 'Se envió un link a tu correo para reestablecer contraseña',
              buttons: ['Ok']
            });
                alert.present()

            this.navCtrl.push(LoginPage);
          }
        })
      } else {
        let alert = this.alertCtrl.create({
              title: 'Recuperar Contraseña',
              message: 'La dirección de correo que ingresaste no existe',
              buttons: ['Ok']
            });
                alert.present()
      }
    });     
    
    
  }  
    
}

  

