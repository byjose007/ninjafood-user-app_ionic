import { Component } from '@angular/core';
import { Validators, ControlGroup, FormBuilder } from '@angular/common';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthspaServices } from '../../providers/authspa/authspa';
import { HomePage } from '../home/home';
import {Constants} from '../../constantes/constants';

@Component({
  templateUrl: 'build/pages/perfil/perfil.html',
  providers: [AuthspaServices, Constants]
})
export class PerfilPage {

  usuario: any;

  //Controles
  formPerfil: ControlGroup  
  
  constructor(
    private navCtrl: NavController, 
    private formBuilder: FormBuilder, 
    private params: NavParams, 
    private authspaServices: AuthspaServices, 
    private viewController: ViewController
  ) {
    this.usuario=JSON.parse(this.params.get('usuario'));  

    this.formPerfil = this.formBuilder.group({
      nombre: [this.usuario.nombre, Validators.required],
      apellido: [this.usuario.apellido, Validators.required],
      cedula: [this.usuario.cedula, Validators.compose([ Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')])],
    });
  }

  actualizarUsuario(){       
    this.authspaServices.actualizarUsuario(this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.cedula)
      .then(data => {
        var user = data;                
        this.usuario = {
          id: user[0].id_usuario.id,
          email: user[0].id_usuario.email,
          nombre: user[0].id_usuario.first_name,
          apellido: user[0].id_usuario.last_name,
          foto: this.usuario.foto,
          redsocial: this.usuario.redsocial,
          cedula: user[0].cedula
        };   
        this.viewController.dismiss({usuario: JSON.stringify(this.usuario)});
      }).catch(error => {        
        console.log(error);
      });
  }

  // atras(){
  //   this.viewController.dismiss({usuario: JSON.stringify(this.usuario)});
  // }

  onPageBack(){
    this.navCtrl.pop();
  }

}
