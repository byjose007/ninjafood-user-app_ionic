import { Component } from '@angular/core';
import { NavController, NavParams, Storage, LocalStorage, LoadingController, Platform, AlertController } from 'ionic-angular';
import { Validators, ControlGroup, FormBuilder } from '@angular/common';
import { Facebook, GooglePlus, SQLite, Device } from 'ionic-native';
import { CarritoPage } from '../../pages/carrito/carrito';
import { HomePage } from '../home/home';
import { AuthspaServices } from '../../providers/authspa/authspa';
import { CustomValidations } from '../../theme/CustomValidations';
import {LicensesPage } from '../licenses/licenses';
import {PswReset} from '../pswReset/pswReset';

@Component({
  templateUrl: 'build/pages/login/login.html',
  providers: [AuthspaServices]
})
export class LoginPage extends CustomValidations {
  mensajeLoginUsuario = "";
  mensajeUsuarioRepeat = "";
  mensajeEmailRepeat = "";
  mensajeCedulaRepeat = "";

  booleanUsuario: boolean = false;
  booleanEmail: boolean = false;
  booleanCedula: boolean = false;

  usuarioNuevo: any = {
    //usuario: null,
    nombres: null,
    apellidos: null,
    cedula: null,
    email: null,
    password: null,
    confirmPassword: null,
  };

  userGa: any = {
    email: null,
    pass: null
  };
  formUsuarioGa: ControlGroup;
  formUsuarioNuevo: ControlGroup;

  usuario: any = null;
  exit: boolean;
  local: any;
  localStorage: any;
  start: string = 'loginPage';
  loginPassword: boolean = false;
  private id: string;
  private rs: any;
  private isChecked:boolean=false;

  constructor(
    protected navCtrl: NavController,
    private formBuilder: FormBuilder,
    private params: NavParams,
    private authspaServices: AuthspaServices,
    private loadingCtrl: LoadingController,
    protected platform: Platform,
    public alertCtrl: AlertController
  ) {
    super(navCtrl, platform);
    this.formUsuarioGa = this.formBuilder.group({
      email: [this.userGa.email, Validators.required],
      pass: [this.userGa.pass, Validators.required]
    });

    this.formUsuarioNuevo = this.formBuilder.group(
      {
        //usuario: [this.usuarioNuevo.usuario, Validators.required],
        nombres: [this.usuarioNuevo.nombres, Validators.required],
        apellidos: [this.usuarioNuevo.apellidos, Validators.required],
        cedula: [this.usuarioNuevo.cedula, Validators.required],
        email: [this.usuarioNuevo.email, Validators.required],
        matchingPassword: this.formBuilder.group({
          password: [this.usuarioNuevo.password, Validators.required],
          confirmPassword: [this.usuarioNuevo.confirmPassword, Validators.required]
        }, { validator: this.areEqual })
      }
    );

    this.id=Device.device.serial;

    /*this.statusFacebook().then(res =>{
      if(res == 0)
        this.statusGoogle().then(resG =>{
          if(resG == 0){
            this.statusLoginUserEmail();
          }
        });
    });*/
  }

  areEqual(group: ControlGroup): any {
    let val;
    let valid = true;

    for (var name in group.controls) {
      if (val === undefined) {
        val = group.controls[name].value
      } else {
        if (val !== group.controls[name].value) {
          valid = false;
          break;
        }
      }
    }

    if (valid) {
      return null;
    }
    return {
      areEqual: true
    };
  }

  loginFacebook() {
    console.log("isChecked: "+this.isChecked);
    if(this.isChecked){
      this.checkNetwork();
      let loader = this.loadingCtrl.create({
        content: "Cargando..."
      });
      loader.present();
      Facebook.login(['public_profile', 'email'])
        .then(rta => {
          if (rta.status == 'connected') {
            Facebook.api('/me?fields=id,name,email,first_name,last_name,gender,picture', ['public_profile', 'email'])
              .then(rta => {
                console.log("email "+rta.email+" first_name: "+rta.first_name+" last_name: "+rta.last_name);
                this.authspaServices.verificarUsuario(rta.email, rta.first_name, rta.last_name)
                  .then(data => {
                    var user = data;
                    this.usuario = {
                      id_usuario: user[0].id_usuario.id,
                      id_perfil: user[0].id_perfil,
                      email: user[0].id_usuario.email,
                      nombre: user[0].id_usuario.first_name,
                      apellido: user[0].id_usuario.last_name,
                      foto: rta.picture.data.url,
                      redsocial: "facebook",
                      cedula: user[0].cedula,
                      tel: user[0].telefono,
                      verificado: user[0].cuentaVerif
                    };
                    this.updateLocalDB(this.usuario.id_usuario, null, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula, this.usuario.tel);
                    this.navCtrl.setRoot(HomePage, { usuario: JSON.stringify(this.usuario) });
                  }).catch(error => {
                    console.log(error);
                  });
              })
              .catch(error => {
                console.log(error);
                loader.dismiss();
              });
          };
        })
        .catch(error => {
          console.log(error);
          loader.dismiss();
        });
      loader.dismiss();
    } else
      this.showAlertLicenses();
  }

  statusFacebook() {
    return new Promise(resolve =>{
      // let loader = this.loadingCtrl.create({
      //   content: "Cargando..."
      // });
      // loader.present();
      this.checkNetwork();
      Facebook.getLoginStatus()
        .then(rta => {
          if (rta.status == 'connected') {
            Facebook.api('/me?fields=id,name,email,first_name,last_name,gender,picture', ['public_profile', 'email'])
              .then(rta => {
                this.authspaServices.verificarUsuario(rta.email, rta.first_name, rta.last_name)
                  .then(data => {
                    var user = data;
                    this.usuario = {
                      id_usuario: user[0].id_usuario.id,
                      email: user[0].id_usuario.email,
                      nombre: user[0].id_usuario.first_name,
                      apellido: user[0].id_usuario.last_name,
                      foto: rta.picture.data.url,
                      redsocial: "facebook",
                      cedula: user[0].cedula,
                      tel: user[0].telefono
                    };
                    console.log(user);
                    // alert(user);
                    // alert(this.usuario);
                    this.updateLocalDB(this.usuario.id_usuario, null, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula, this.usuario.tel);
                    resolve(1);
                    this.navCtrl.setRoot(HomePage, { usuario: JSON.stringify(this.usuario) });
                  }).catch(error => {
                    console.log(error);
                    resolve(0);
                  });
              })
              .catch(error => {
                //alert(error);     
                console.log(error);
                resolve(0);
                // loader.dismiss();        
              });
          } else
            resolve(0);
        }).catch(error => {
          // alert(error);
          console.log(error);
          resolve(0);
          // loader.dismiss();
        });
      // loader.dismiss();
    });    
  }

  loginGoogle() {
    console.log("isChecked: "+this.isChecked);
    if(this.isChecked){
      this.checkNetwork();
      let loader = this.loadingCtrl.create({
        content: "Cargando..."
      });
      loader.present();
      GooglePlus.login(
        {}
      ).then(rta => {
        this.authspaServices.verificarUsuario(rta.email, rta.givenName, rta.familyName)
          .then(data => {
            var user = data;
            this.usuario = {
              id_usuario: user[0].id_usuario.id,
              id_perfil: user[0].id_perfil,
              email: user[0].id_usuario.email,
              nombre: user[0].id_usuario.first_name,
              apellido: user[0].id_usuario.last_name,
              foto: rta.imageUrl,
              redsocial: "google",
              cedula: user[0].cedula,
              tel: user[0].telefono,
              verificado: user[0].cuentaVerif
            };
            console.log(user);
            this.updateLocalDB(this.usuario.id_usuario, null, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula, this.usuario.tel);
            this.navCtrl.setRoot(HomePage, { usuario: JSON.stringify(this.usuario) });
          }).catch(error => {
            console.log(error);
          });
      })
        .catch(error => {
          // console.log(error);
          loader.dismiss();
        });
      loader.dismiss();
    } else
      this.showAlertLicenses();
  }

  statusGoogle() {
    return new Promise(resolve => {
      GooglePlus.trySilentLogin(
        {}
      ).then(rta => {
        if (rta != "logged out") {
          this.authspaServices.verificarUsuario(rta.email, rta.givenName, rta.familyName)
            .then(data => {
              var user = data;
              this.usuario = {
                id_usuario: user[0].id_usuario.id,
                email: user[0].id_usuario.email,
                nombre: user[0].id_usuario.first_name,
                apellido: user[0].id_usuario.last_name,
                foto: rta.imageUrl,
                redsocial: "google",
                cedula: user[0].cedula,
                tel: user[0].telefono
              };
              console.log(user);
              this.updateLocalDB(this.usuario.id_usuario, null, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula, this.usuario.tel);
              resolve(1);
              this.navCtrl.setRoot(HomePage, { usuario: JSON.stringify(this.usuario) });
            }).catch(error => {
              console.log(error);
              resolve(0);
            });
        } else
          resolve(0);
      }).catch(error => {
        // alert(error);
        console.log(error);
        resolve(0);
        // loader.dismiss();  
      });
    });
  }


  public gotoHome() {
    this.authspaServices.verificarUsuario("clientePruebas@gmail.com", "Cliente", "Pruebas")
      .then(data => {
        var user = data;
        // console.log(user);
        this.usuario = {
          id_usuario: user[0].id_usuario.id,
          id_perfil: user[0].id_perfil,
          email: user[0].id_usuario.email,
          nombre: user[0].id_usuario.first_name,
          apellido: user[0].id_usuario.last_name,
          foto: "http://megaicons.net/static/img/icons_sizes/311/1406/256/mario-icon.png",
          redsocial: "default",
          cedula: user[0].cedula,
          tel: user[0].telefono
        };
        this.updateLocalDB(this.usuario.id_usuario, this.usuario.id_perfil, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula, this.usuario.tel);
        this.navCtrl.setRoot(HomePage, { usuario: JSON.stringify(this.usuario) });
      }).catch(error => {
        console.log(error);
        this.checkServerResponse(error);
      });
  }

  public loginGoodAppetit() {
    this.checkNetwork().then(net => {
      console.log("[loginGoodAppetit] net: "+net);
      if(net == 1){
        let loader = this.loadingCtrl.create({
          content: "Cargando..."
        });
        loader.present();
        this.authspaServices.loginUsuario(this.userGa)
          .then(data => {
            var user = data;
            this.usuario = {
              id_usuario: user[0].id_usuario.id,
              id_perfil: user[0].id_perfil,
              email: user[0].id_usuario.email,
              nombre: user[0].id_usuario.first_name,
              apellido: user[0].id_usuario.last_name,
              foto: user[0].ruta_foto,
              redsocial: "default",
              cedula: user[0].cedula,
              tel: user[0].telefono,
              verificado: user[0].cuentaVerif
            };
            console.log("Login Email Password save in storage ");
            this.updateLocalDB(this.usuario.id_usuario, this.usuario.id_perfil, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula, this.usuario.tel);
            console.log(JSON.stringify(this.usuario));
            this.navCtrl.setRoot(HomePage, { usuario: JSON.stringify(this.usuario) });
            loader.dismiss();
          }).catch(error => {
            this.mensajeLoginUsuario = "Usuario o contraseña inválidos";
            console.log(error);
            loader.dismiss();
          });
      }
    });
  }

  statusLoginUserEmail() {
    this.queryUser().then(rta => {
      this.rs = rta;
      if (rta != undefined) {
        if(this.rs.registro == '1'){
          this.authspaServices.verificarUsuario(this.rs.email, this.rs.first_name, this.rs.last_name)
            .then(data => {
              var user = data;
              this.usuario = {
                id_usuario: user[0].id_usuario.id,
                email: user[0].id_usuario.email,
                nombre: user[0].id_usuario.first_name,
                apellido: user[0].id_usuario.last_name,
                foto: user[0].ruta_foto,
                redsocial: "default",
                cedula: user[0].cedula
              };
              //console.log(user);
              //this.updateLocalDB(this.usuario.id_usuario, null, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula);
              this.navCtrl.setRoot(HomePage, { usuario: JSON.stringify(this.usuario) });
            }).catch(error => {
              console.log(error);
            });
        }
      }
    }).catch(error => {
      // alert(error);
      console.log(error);
      // loader.dismiss();  
    });
  }

  public registrarUsuarioGoodAppetit() {
    console.log("isChecked: "+this.isChecked);
    if(this.isChecked){
      this.checkNetwork().then(net => {
      console.log("[registrarUsuarioGoodAppetit] net: "+net);
      if(net == 1){
        let loader = this.loadingCtrl.create({
          content: "Cargando..."
        });
        loader.present();
        this.authspaServices.registrarUsuario(this.usuarioNuevo)
          .then(data => {
            var user = data;
            let usuarioRespuesta: any;
            usuarioRespuesta = data;
            if (usuarioRespuesta.length == 0) {
              console.log("Error al registrar usuario");
            }else{
              this.usuario = {
                id_usuario: user[0].id_usuario.id,
                id_perfil: user[0].id_perfil,
                email: user[0].id_usuario.email,
                nombre: user[0].id_usuario.first_name,
                apellido: user[0].id_usuario.last_name,
                foto: user[0].ruta_foto,
                redsocial: "default",
                cedula: user[0].cedula,
                tel: user[0].telefono
              };
              this.updateLocalDB(this.usuario.id_usuario, this.usuario.id_perfil, this.usuario.email, this.usuario.nombre, this.usuario.apellido, this.usuario.foto, this.usuario.redsocial, this.usuario.cedula, this.usuario.tel);
              loader.dismiss();
              this.navCtrl.setRoot(HomePage, { usuario: JSON.stringify(this.usuario) });
            }        
          }).catch(error => {
            console.log(error);
            loader.dismiss();
          });
        }
      });
    } else
      this.showAlertLicenses();
  }

  public comprobarUsuarioGoodAppetit() {
    this.authspaServices.comprobarUsuario(this.usuarioNuevo)
      .then(data => {
        let usuarioRespuesta: any;
        usuarioRespuesta = data;
        if (usuarioRespuesta.length != 0) {
          this.booleanUsuario= true;
          this.mensajeUsuarioRepeat = "El nombre de usuario ingresado ya se encuentra registrado";
        } else {
          this.booleanUsuario= false;
          this.mensajeUsuarioRepeat = "";
        }
      }).catch(error => {
        console.log(error);
      });
  }

  public comprobarCedulaGoodAppetit() {
    if (this.usuarioNuevo.cedula.length == 10) {
      this.authspaServices.comprobarCedula(this.usuarioNuevo)
        .then(data => {
          let usuarioRespuesta: any;
          usuarioRespuesta = data;
          if (usuarioRespuesta.length != 0) {
            this.booleanCedula= true;
            this.mensajeCedulaRepeat = "El numero de cédula ingresado ya se encuentra registrado";
          } else {
            this.booleanCedula= false;
            this.mensajeCedulaRepeat = "";
          }
        }).catch(error => {
          console.log(error);
        });
    }
  }

  public comprobarEmailGoodAppetit() {
    this.authspaServices.comprobarEmail(this.usuarioNuevo)
      .then(data => {
        let usuarioRespuesta: any;
        usuarioRespuesta = data;
        if (usuarioRespuesta.length != 0) {
          this.booleanEmail= true;
          this.mensajeEmailRepeat = "El email ingresado ya se encuentra registrado";
        } else {
          this.booleanEmail= false;
          this.mensajeEmailRepeat = "";
        }
      }).catch(error => {
        console.log(error);
      });
  }

  public showPasswordForm(){
    if (this.loginPassword)
      this.loginPassword = false;
    else
      this.loginPassword = true;
  }

  public updateLocalDB(id_usuario:String, id_perfil:String, email:String, first_name:String, last_name:String, foto:String, 
  redsocial:String, cedula:String, telefono:String){
    console.log("####### LOGIN.TS #######");
    let db = new SQLite();
    let sqlInsertSesion = 'UPDATE startApp set registro="1", id_usuario="'+id_usuario+'", id_perfil="'+id_perfil+'", email="'+email+'", first_name="'+first_name
    +'", last_name="'+last_name+'", foto="'+foto+'", redsocial="'+redsocial+'", telefono="'+telefono+'" WHERE token=?';
    db.openDatabase({
      name: 'goodappetit.db',
      location: 'default' // the location field is required
    }).then(rta => {
      console.log("id: "+this.id);
      db.executeSql(sqlInsertSesion, [this.id]).then(rta=>{
        console.log("regsitrado");
      }, (err)=>{
        console.error(JSON.stringify(err));
        console.error("Error: "+err);
      })
    });
  }

  public queryUser(){
    let db = new SQLite();
    let sqlSelectSesion = 'SELECT * FROM startApp WHERE token=?';
    return new Promise(resolve =>{
      db.openDatabase({
        name: 'goodappetit.db',
        location: 'default' // the location field is required
      }).then(rta => {
        console.log("id: "+this.id);
        db.executeSql(sqlSelectSesion, [this.id]).then(rta=>{
          resolve(rta.rows.item(0));
        }, (err)=>{
          console.error(JSON.stringify(err));
          console.error("Error: "+err);
        })
      });
    });
  }

  public goToLicenses(){
    console.log("goToLicense: .I.");
    this.navCtrl.push(LicensesPage);
  }

  showAlertLicenses() {
    let alert = this.alertCtrl.create({
      title: 'Terminos y Condiciones!',
      subTitle: 'Aceptar los Terminos y Condiciones antes de continuar',
      buttons: ['OK']
    });
    alert.present();
  }

  goToPswReset(){
    this.navCtrl.push(PswReset);
  }

}
