
import {Component, Input, ViewChild, Renderer, Query, QueryList, ElementRef} from '@angular/core';
import {Geolocation, Device} from 'ionic-native';
import {IONIC_DIRECTIVES, NavParams} from 'ionic-angular';
import {TimerWrapper} from '@angular/core/src/facade/async';
import {URLSearchParams, Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

declare var ol: any;
var misParametros: any;

@Component({
	selector: 'map-component',
	templateUrl: 'build/pages/map/map.html',
	directives: [IONIC_DIRECTIVES]
})

export class MapComponent {
	lon: number;
	lat: number;
	pedidos: any[] = [];
	pedido: any;
	con: any;
	@ViewChild('map') map;

	constructor(public renderer: Renderer, public params: NavParams, private http: Http) {
		this.lon = this.params.get('lon');
		this.lat = this.params.get('lat');
		this.getUbicacion().then(ub => {
			console.log("ub: "+ub);
			this.ngAfterViewInitX();
		});		
		/*TimerWrapper.setTimeout(() => {
			this.ngAfterViewInitX();
		}, 1000);*/
		
	}
	getUbicacion() {
		return new Promise(resolve =>{
			if (this.lon == undefined || this.lat == undefined) {
				Geolocation.getCurrentPosition().then(pos => {
					this.lat = pos.coords.latitude;
					this.lon = pos.coords.longitude;
					console.log("this.lat / this.lon: "+this.lat +" / "+this.lon);
					resolve(1);
				}, function (error) {
					console.log("Error inesperado "+ error);
					resolve(0);
				});
			}
		});
	}
	ngAfterViewInitX() {

		if (this.lon == undefined) {
			console.log("EN CASO DE QUE lat Y log NO TENGA DATOS");
			this.lon = -79.20156127945656;
			this.lat = -3.996397160169863;
		}

		var points = [],
			msg_el = document.getElementById('msg'),
			url_osrm_nearest = 'https://router.project-osrm.org/nearest/v1/driving/',
			url_osrm_route = 'https://router.project-osrm.org/route/v1/driving/',
			icon_url_suc = 'image/sucursal1.png',
			icon_url_usr = 'image/redMarker.jpeg',
			icon_url_tra = 'image/transportista.png',
			vectorSource = new ol.source.Vector(),
			vectorLayer = new ol.layer.Vector({
				source: vectorSource
			}),
			stylesS = {
				route: new ol.style.Style({
					stroke: new ol.style.Stroke({
						width: 4, color: '#04B431'
					})
				}),
				icon: new ol.style.Style({
					image: new ol.style.Icon({
						anchor: [0.5, 0.5],
						src: icon_url_suc
					})
				})
			},
			stylesT = {
				route: new ol.style.Style({
					stroke: new ol.style.Stroke({
						width: 4, color: '#04B431'
					})
				}),
				icon: new ol.style.Style({
					image: new ol.style.Icon({
						anchor: [0.5, 0.5],
						src: icon_url_tra
					})
				})
			},
			styles = {
				route: new ol.style.Style({
					stroke: new ol.style.Stroke({
						width: 4, color: '#04B431'
					})
				}),
				icon: new ol.style.Style({
					image: new ol.style.Icon({
						anchor: [0.5, 0.5],
						src: icon_url_usr
					})
				})
			};

		var map = new ol.Map({
			target: 'map',
			layers: [
				new ol.layer.Tile({
					source: new ol.source.OSM()
				}),
				vectorLayer
			],
			view: new ol.View({
				center: ol.proj.transform([this.lon, this.lat], 'EPSG:4326', 'EPSG:900913'),
				zoom: 15
			})
		});
		console.log("centrando mapa en direccion: "+this.lon + this.lat);		
		var utils = {
			getNearest: function (coord) {
				var coord4326 = utils.to4326(coord);
				return new Promise(function (resolve, reject) {

					service.get(url_osrm_nearest + coord4326.join())
						.map(res => res.json())
						.subscribe(datap => {
							if (datap.code === 'Ok')
								resolve(datap.waypoints[0].location);
							else reject()
							//return datap;
						},
						error => {
							console.error("Error saving food!");
							return Observable.throw(error);
						});

				});
			},
			createFeature: function (coord, img) {
				var feature = new ol.Feature({
					type: 'place',
					geometry: new ol.geom.Point(ol.proj.fromLonLat(coord))
				});
				if (img == 1)
					feature.setStyle(styles.icon);
				else
					if (img == 2)
						feature.setStyle(stylesS.icon);
					else
						feature.setStyle(stylesT.icon);
				vectorSource.clear()
				vectorSource.addFeature(feature);
			},
			createRoute: function (polyline, img) {
				// route is ol.geom.LineString
				var route = new ol.format.Polyline({
					factor: 1e5
				}).readGeometry(polyline, {
					dataProjection: 'EPSG:4326',
					featureProjection: 'EPSG:3857'
				});
				var feature = new ol.Feature({
					type: 'route',
					geometry: route
				});
				feature.setStyle(styles.route);
				vectorSource.addFeature(feature);
				points = [];
			},
			to4326: function (coord) {
				return ol.proj.transform([
					parseFloat(coord[0]), parseFloat(coord[1])
				], 'EPSG:3857', 'EPSG:4326');
			},
			to38576: function (coord) {
				return ol.proj.transform([
					parseFloat(coord[0]), parseFloat(coord[1])
				], 'EPSG:4326', 'EPSG:900913');
			}
		};

		let service: any = this.http;

		function routeMap(pos_actual, img) {
			pos_actual = utils.to38576(pos_actual);
			utils.getNearest(pos_actual).then(function (coord_street) {
				var last_point = points[points.length - 1];
				var points_length = points.push(coord_street);
				utils.createFeature(coord_street, img);
				var coord6 = utils.to38576(coord_street);
				if (points_length < 2) {
					return;
				}

				var point1 = last_point.join();
				var point2 = coord_street[0] + "," + coord_street[1];

				service.get(url_osrm_route + point1 + ';' + point2)
					.map(res => res.json())
					.subscribe(datap => {
						if (datap.code !== 'Ok') {
							msg_el.innerHTML = 'No route found.';
							return;
						}
						utils.createRoute(datap.routes[0].geometry, img);
					},
					error => {
						console.error("Error saving food!");
						return Observable.throw(error);
					});


			});
		}
		var coord_street = [this.lon, this.lat];
		//utils.createFeature(coord_street, 1);
		var center = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
		this.lon = center[0];
		this.lat = center[1];
		this.params.data["lon"] = this.lon;
		this.params.data["lat"] = this.lat;
		misParametros=this.params;

		map.on('moveend', function(e) {
			var center = ol.proj.transform(e.map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
			var coord_street = [center[0], center[1]];
			this.lon = center[0];
			this.lat = center[1];
			console.log("moveend: "+this.lat+" # "+this.lon);
			this.params=misParametros;
			this.params.data["lon"] = this.lon;
			this.params.data["lat"] = this.lat;
			//utils.createFeature(coord_street, 1);
		});
		/*map.on('movestart', function(e) {
			var center = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
			var coord_street = [center[0], center[1]];
			utils.createFeature(coord_street, 1);
		});*/	

		/*map.on('pointermove', function(e) {
		if (e.dragging) {
			var center = ol.proj.transform(e.map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
			var coord_street = [center[0], center[1]];
			console.log("-->" +coord_street[0]+"-->"+coord_street[1]);
			this.lon=coord_street[0];
			this.lat=coord_street[1];
			
			this.params.data["lon"] = this.lon;
			this.params.data["lat"] = this.lat;
			// con.latitud=this.lat;
			// con.longitud=this.lon;

			// this.con.setLocation(this.lat, this.lon);

			//utils.createFeature(coord_street, 1);
		}
		//var pixel = map.getEventPixel(evt.originalEvent);
		//displayFeatureInfo(pixel);
		});*/
		
	}
}