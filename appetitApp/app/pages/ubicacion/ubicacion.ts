import { Query, QueryList, Component, ElementRef } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Geolocation, Device, Diagnostic } from 'ionic-native';
import { MapComponent } from '../map/map';
import { formEntregaPage } from '../form-entrega/form-entrega';
import { ProveedorService } from '../../providers/proveedor-service/proveedor-service';
import { TimerWrapper } from '@angular/core/src/facade/async';
@Component({
  templateUrl: 'build/pages/ubicacion/ubicacion.html',
  directives: [MapComponent],
  providers: [ProveedorService]
})
export class UbicacionPage {
  lista_productos: any[];
  sub_total: number = 0;
  precio_envio: number = 0;
  total: number = 0;
  tipo_envio: string;
  tipo_pago: string;
  comentario: string;
  sucursal: any;
  id_usuario: any;
  lon: number;
  lat: number;
  id_direccion: any;
  id_perfil: any;
  nuevaDireccion: any = {};
  constructor(public navCtrl: NavController, public NavParams: NavParams,
    public proveedorService: ProveedorService,
    public alertCtrl: AlertController) {
    this.total = 0;
    this.sub_total = 0;
    this.tipo_pago = this.NavParams.get('tipo_pago');
    this.tipo_envio = this.NavParams.get('tipo_envio');
    this.lista_productos = this.NavParams.get('lista_productos');
    this.precio_envio = this.NavParams.get('precio_envio');
    this.sucursal = this.NavParams.get('sucursal');
    this.comentario = this.NavParams.get('comentario');
    this.id_usuario = this.NavParams.get('id_usuario');
    this.lista_productos.forEach(p => { this.sub_total += p.precio });
    this.total = (this.sub_total + this.precio_envio);
  }

  gotoConfirmarCompra() {
    var params = this.NavParams;
    console.log("METODO ACEPTAR DIRECCION");
    console.log(this.NavParams.get('lat') + '/' + this.NavParams.get('lon'));
    //this.navCtrl.pop();

    /*let prompt = this.alertCtrl.create({
      title: 'Login',
      message:
      +"<ion-content>"
      +"<ion-list>"
      +"<ion-item>"
			+"<ion-input required [(ngModel)]='calle_principal' type='text'></ion-input>"
		  +"</ion-item>"
      +"</ion-list>"
      +"</ion-content>"
      ,                                           
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();*/

    let confirm = this.alertCtrl.create(
    {
      title: 'Nueva Dirección',
      //message: 'Confirmar direccion de entrega',
      inputs: [
        {
          name: 'calle_principal',
          type: 'text',
          label: 'Bespin',
          placeholder: 'Calle principal'
        },
        {
          name: 'calle_secundaria',
          type: 'text',
          label: 'Bespin',
          placeholder: 'Calle secundaria'
        },
        {
          name: 'referencia',
          type: 'text',
          label: 'Bespin',
          placeholder: 'Referencia'
        },
        {
          name: 'num_casa',
          type: 'text',
          label: 'Bespin',
          placeholder: 'Número de casa'
        },
        {
          name: 'celular',
          type: 'number',
          label: 'Bespin',
          placeholder: 'Celular'
        }             
      ],
      buttons: [
        {
          text: 'Guardar',
          handler: data => {
            return this.validarForm(data.calle_principal, data.calle_secundaria,
              data.referencia, data.num_casa, data.celular, false);
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            console.log('dirección cancelada');
          }
        }
      ]
    });

    confirm.present();
  }

  public gotoCerrar() {
    let num_views: number;
    num_views = this.navCtrl.length();
    this.navCtrl.pop();
  }

  public validarForm(calle_principal, calle_secundaria, referencia, num_casa, celular, firtsForm) {

    let celStr: string = celular.toString();
    let cel = celStr.substr(0, 2);

    if (calle_principal == "" || calle_secundaria == ""
      || referencia == "" || num_casa == "" || celular == "") {
        this.mansajeAlert("Error", "Todos los datos son obligatorios");
      return false;
    } else if (cel != "09") {
        this.mansajeAlert("Error", "Introduce un número de celular válido");
        return false;
    } else {
      if (firtsForm) { // Si es la primera compra
        this.guardarDireccion(this.id_usuario, calle_principal, calle_secundaria, referencia, num_casa, celular, true);
        return true;
      } else
        this.guardarDireccion(this.id_usuario, calle_principal, calle_secundaria, referencia, num_casa, celular, false);
        return true;
    }

  }
  public guardarDireccion(id_usuario, calle_principal, calle_secundaria, referencia, num_casa, celular, firtsDir) {

    this.proveedorService.getPerfil(id_usuario)
      .then(perfil => {
        let profile: any = perfil;
        this.id_perfil = profile[0].id_perfil;

        if (firtsDir)
          this.validarDireccion(calle_principal, calle_secundaria, referencia, num_casa, celular, true);
        else
          this.validarDireccion(calle_principal, calle_secundaria, referencia, num_casa, celular, false);
      });

  }

  
  public mansajeAlert(titulo, texto) {
    let confirm = this.alertCtrl.create({
      title: titulo,
      message: texto
    });
    confirm.present();
    TimerWrapper.setTimeout(() => {
      confirm.dismiss();
    }, 2100);
  }
  public validarDireccion(calle1, calle2, referencia, num_casa, celular, firtsDir) {

    //this.nuevaDireccion.calle_principal = calle1;


    this.nuevaDireccion = {
      "calle_principal": calle1,
      "calle_secundaria": calle2,
      "numero_casa": num_casa,
      "referencia": referencia,
      "latitud": this.NavParams.get('lat'),
      "longitud": this.NavParams.get('lon'),
      "id_perfil": this.id_perfil
    }

    this.proveedorService.guardarDireccion(this.nuevaDireccion).then(data => {
      if (firtsDir) {
        let dir: any = data;
        //this.getDirecciones();
        this.id_direccion = dir.id_direccion;
      } else {
        //this.getDirecciones();
      }
      this.proveedorService.getDirecciones(this.id_usuario)
        .then(data => {
          this.nuevaDireccion = data;
          if (this.nuevaDireccion.length > 1) {
            let num_views: number;
            num_views = this.navCtrl.length();
            this.navCtrl.remove(num_views - 1);
            this.navCtrl.pop();
            
            console.log('Funcion validar direccion')
            console.log(this.comentario)

            this.navCtrl.push(formEntregaPage, {
              id_usuario: this.id_usuario, lista_productos: this.lista_productos, precio_envio: this.precio_envio, tipo_pago: this.tipo_pago,
              tipo_envio: this.tipo_envio, sucursal: this.sucursal, comentario: this.comentario
            });
          }
          else
            this.navCtrl.push(formEntregaPage, {
              id_usuario: this.id_usuario, lista_productos: this.lista_productos, precio_envio: this.precio_envio, tipo_pago: this.tipo_pago,
              tipo_envio: this.tipo_envio, sucursal: this.sucursal, comentario: this.comentario
            });
        });
      //data.forEach(p => p.fecha = p.fecha.substring(11,19) );
    });
  }

}
