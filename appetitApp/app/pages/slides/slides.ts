import {Component} from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { SQLite, Device } from 'ionic-native';

/*
  Generated class for the SlidesPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/slides/slides.html',
})
export class SlidesPage {
  private id: string;
  slides = [
    {
      titulo: "",
      descripcion: "",//Como te gusta a ti!!",
      imagen: "../www/build/images/logo_blanco-rojo.png",
    },
    {
      titulo: "GoodAppetit",
      descripcion: "GoodAppetit, es la nueva solución para perdir comida a domilio, Elige un restaurante y paga en efectivo en el momento de la entrega."
      //imagen: "http://previews.123rf.com/images/neyro2008/neyro20081403/neyro2008140300122/27163470-search-order-pay-deliver-icons-in-flat-design-style-for-online-shop-Stock-Vector.jpg",
    }/*,
    {
      titulo: "GoodAppetit",
      descripcion: "Para utilizar GoodAppetit es necesario que te registres de forma rápida con tu cuenta de Facebook o Google",
      imagen: "https://www.ajamar.com/static/home/img/landing-page-1/iphone6.png",
    }*/
  ]

  constructor(private navCtrl: NavController, private loadingCtrl: LoadingController) {
    this.id=Device.device.serial;    
    this.crearTabla();
  }
  
  iniciar(){
    this.navCtrl.setRoot( LoginPage );
  }

  crearTabla(){    
    //lockOrientation('portrait');
    console.log("####### SLIDER.TS #######");
    let loader = this.loadingCtrl.create({
      content: "Cargando..."
    });
    loader.present();
    let db = new SQLite();
    let sqlCreateTable = 'CREATE TABLE IF NOT EXISTS startApp (token VARCHAR(32), registro VARCHAR(1), id_usuario VARCHAR(10), id_perfil VARCHAR(10), email VARCHAR(50), first_name VARCHAR(50), last_name VARCHAR(50), foto VARCHAR(500), redsocial VARCHAR(50), cedula VARCHAR(11), telefono VARCHAR(20))';
    let sqlQuerySesion = 'SELECT * FROM startApp WHERE token=?';    
    let sqlInsertSesion = 'INSERT INTO startApp (token, registro) VALUES (?,?)';
    db.openDatabase({
      name: 'goodappetit.db',
      location: 'default' // the location field is required
    }).then(rta => {      
      db.executeSql(sqlCreateTable, {}).then(() => {
        console.log("id: "+this.id);
        db.executeSql(sqlQuerySesion, [this.id]).then(rta=>{
          if(rta.rows.length==0){
            db.executeSql(sqlInsertSesion, [this.id, '0']).then(rta=>{
              console.log(JSON.stringify(rta));
              loader.dismiss();
            }, (err)=>{
              console.error(JSON.stringify(err));
              loader.dismiss();
            })
          }else{
            loader.dismiss();
            this.iniciar();
          }
        }, (err)=>{          
          console.error(JSON.stringify(err));
          loader.dismiss();
        })
      }, (err) => {
        console.error(JSON.stringify(err));
        loader.dismiss();
      });
    }, (err) => {
      console.error(JSON.stringify(err));
      loader.dismiss();
    });
  }

}
