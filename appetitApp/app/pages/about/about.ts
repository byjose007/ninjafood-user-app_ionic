import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {LicensesPage } from '../licenses/licenses';

@Component({
  templateUrl: 'build/pages/about/about.html'
})
export class AboutPage {
  constructor(private navCtrl: NavController) {
  }
  public goToLicenses(){
    console.log("goToLicense: .I.");
    this.navCtrl.push(LicensesPage);
  }
}
