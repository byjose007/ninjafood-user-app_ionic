import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import {HomePage} from '../home/home';

@Component({
  templateUrl: 'build/pages/intro/intro.html',
})
export class IntroPage {

  constructor(private navCtrl: NavController) {
    this.navCtrl = navCtrl;

  }

  goToHome(){
      this.navCtrl.setRoot(HomePage);
  }

}
