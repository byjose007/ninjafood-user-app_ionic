import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform, ToastController } from 'ionic-angular';
import {PlatoModel} from '../../providers/models/plato-model';
import {formEntregaPage} from '../form-entrega/form-entrega';
import {UbicacionPage} from '../ubicacion/ubicacion';
import { ProveedorService } from '../../providers/proveedor-service/proveedor-service';
import { AuthspaServices } from '../../providers/authspa/authspa';

/*
  Generated class for the CarritoPage page.
  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare var window: any;

@Component({
  templateUrl: 'build/pages/carrito/carrito.html',
  providers: [ProveedorService, AuthspaServices]
})
export class CarritoPage {
  lista_productos: any[];
  sub_total: number = 0;
  precio_envio: number = 0;
  total: number = 0;
  tipo_envio: string;
  tipo_pago: string;
  comentario: string;
  sucursal: any;
  direcciones: any;
  id_usuario: any;
  lon: number;
  lat: number;
  cuentaVerif:boolean;
  constructor(private navCtrl: NavController,
    private NavParams: NavParams,
    private authspaServices: AuthspaServices, 
    private toastCtrl: ToastController,
    public proveedorService: ProveedorService,
    private alertCtrl: AlertController,
    protected platform: Platform) {
    this.total = 0;
    this.sub_total = 0;
    this.lista_productos = this.NavParams.get('lista_productos');
    this.sucursal = this.NavParams.get('sucursal');
    this.id_usuario = this.NavParams.get('id_usuario');
    this.lista_productos.forEach(p => {
      if(p.valor_oferta !== null && p.valor_oferta !==undefined) 
        this.sub_total += p.valor_oferta;
      else
        this.sub_total += p.precio;
    });
    //this.total = (this.sub_total + this.sucursal.precio_envio);
    this.precio_envio = this.sucursal.precio_envio;
    this.obtnerCuentaVerif(this.id_usuario);
  }

  public eliminarPlato(index): void {
    this.total = 0;
    this.sub_total = 0;
    this.lista_productos.splice(index, 1);
    this.lista_productos.forEach(p => { 
      if(p.valor_oferta !== null && p.valor_oferta !==undefined) 
        this.sub_total += p.valor_oferta;
      else
        this.sub_total += p.precio; 
    });
    this.total = (this.sub_total + this.sucursal.precio_envio);
  }

  // Alert escoger tipo de pago && tipo de envio
  public hacerPedido(): void {
    this.proveedorService.getDirecciones(this.id_usuario)
      .then(data => {
        if (!this.tipo_envio) {
          let alert = this.alertCtrl.create({
            title: "Mensaje",
            subTitle: "Por favor elige un tipo de envio",
            buttons: ['OK']
          });
          alert.present();

        } else if (!this.tipo_pago) {
          let alert2 = this.alertCtrl.create({
            title: "Mensaje",
            subTitle: "Por favor elige un tipo de pago",
            buttons: ['OK']
          });
          alert2.present();
        } else {
          this.direcciones = data;
          
          if (this.direcciones.length == 0 && this.tipo_envio == 'domicilio'){
            this.checkServerResponse();

            console.log('Si es a domicilio');
          console.log(this.comentario);
          
            this.navCtrl.push(UbicacionPage, {
              lon: this.lon, lat: this.lat, id_usuario: this.id_usuario,
              lista_productos: this.lista_productos, precio_envio: this.precio_envio, tipo_pago: this.tipo_pago,
              tipo_envio: this.tipo_envio, sucursal: this.sucursal, comentario: this.comentario
            });
          }
          else
          console.log('Si es para recoger');
          console.log(this.comentario);
            this.navCtrl.push(formEntregaPage, {
              id_usuario: this.id_usuario, lista_productos: this.lista_productos, precio_envio: this.precio_envio, tipo_pago: this.tipo_pago,
              tipo_envio: this.tipo_envio, sucursal: this.sucursal, comentario: this.comentario
            });
        }

      });

  }

  
   checkServerResponse() {
        this.platform.ready().then(() => {
            this.showToast("Usuario sin direccion, Crear Dirección", "long", "bottom");
        });
    }
    showToast(message, duration, position) {
        this.platform.ready().then(() => {
            window.plugins.toast.show(message, duration, position);
        });
    }

    validateTotal(ev: any, dlvy_type:any){
      console.log("############## TOTAL: "+dlvy_type);
      if(dlvy_type == 'domicilio')
        this.total = (this.sub_total + this.sucursal.precio_envio);
      else if(dlvy_type == 'recoger')
        this.total = (this.sub_total);
    }

    mensajeCuentaVerificada() {
    const toast = this.toastCtrl.create({
      message: 'Lo sentimos, pero no podrás realizar una comprar hasta que hayas verificado tu cuenta.',
      showCloseButton: true,
      duration: 8000,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

  showLongToast() {
    let toast = this.toastCtrl.create({
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea voluptatibus quibusdam eum nihil optio, ullam accusamus magni, nobis suscipit reprehenderit, sequi quam amet impedit. Accusamus dolorem voluptates laborum dolor obcaecati.',
      duration: 5000,
    });
    toast.present();
  }

  obtnerCuentaVerif(idUsuario){       
    this.authspaServices.getPerfil(idUsuario)
      .then(data => {
        var user = data;    
        console.log(JSON.stringify(user));    
        this.cuentaVerif=user[0].cuentaVerif;        
        // this.userData = {
        //   id: user[0].id_usuario.id,
        //   email: user[0].id_usuario.email,
        //   nombre: user[0].id_usuario.first_name,
        //   apellido: user[0].id_usuario.last_name,
        //   cedula: user[0].cedula,
        //   tel: user[0].telefono,
        //   verificado: user[0].cuentaVerif
        // };   
        // this.viewController.dismiss({usuario: JSON.stringify(this.userData)});
      }).catch(error => {        
        console.log(error);
      });
  }
}
