import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { TimerWrapper } from '@angular/core/src/facade/async';
import { ProveedorService } from '../../providers/proveedor-service/proveedor-service';
import { HomePage } from '../home/home/';
import { Observable } from "rxjs/Rx";
/*
  Generated class for the EstadoPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/pedidos/pedidos.html',
  providers: [ProveedorService]
})
export class PedidoPage {
  tiempo_espera: number = 2;
  segundo: number = 10;
  //minutosucursal: number = 0
  //minutotransportista: number = 1;
  minuto: number;
  minutos: string;
  segundos: string;
  entregado: boolean = false;
  estado: number = 1;
  tiempo: string;
  count: number;
  id_pedido: any;
  id_usuario: any;
  pedido: any;
  primero: boolean = false;
  notificacion1: boolean = false;
  notificacion2: boolean = false;
  listaPedidos = [];
  items = [];
  pagina: number = 0;
  totalPedidos: number;
  id_sucursal: any;
  numPedidos: number;
  reportarPedido: any;
  tipo_sugerencia: any;
  sugerencia: any = {};

  constructor(private NavParams: NavParams, private navCtrl: NavController,
    private alertCtrl: AlertController, private proveedorService: ProveedorService) {

    //this.pedido = this.NavParams.get('pedido');
    //this.items

    this.id_usuario = this.NavParams.get('id_usuario');
    this.reportarPedido = this.NavParams.get('reportar');
    this.tipo_sugerencia = this.NavParams.get('tipo_sugerencia');

    //this.id_pedido = this.pedido.id_pedido;
    this.proveedorService.getTotalPedidos(this.id_usuario)
      .then(pedidos => {
        let arrayData: any;
        arrayData = pedidos;

        this.totalPedidos = arrayData.length;
        this.getListaPedidos(this.id_usuario);
      });
  }

  /*-----------------------------------------*/

  public enviarReporte() {

    if (this.tipo_sugerencia == 'Pedido') {
      var tipo = 'Pedido';
    } else {
      var tipo = 'Entrega';
    }

    let confirm = this.alertCtrl.create({


      title: 'Reportar '+ tipo,
      //message: 'Confirmar direccion de entrega',
      inputs: [
        {
          name: 'motivo',
          type: 'text',
          placeholder: 'Motivo de reporte'
        }
      ],
      buttons: [
        {
          text: 'Enviar',
          handler: data => {
            console.log('reporte enviado' + data.motivo);
            this.guardarSugerencia(data.motivo);
            confirm.destroy();
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            console.log('reporte cancelado');
          }
        }
      ]
    });

    confirm.present();



  }
  /*-----------------------------------------*/

  public guardarSugerencia(observacion) {

    if (this.tipo_sugerencia == 'Pedido') {
      this.sugerencia.tipo = 'Pedido';
    } else {
      this.sugerencia.tipo = 'Servicio_transportista';
    }

    this.sugerencia.observacion = observacion;
    this.sugerencia.estado = 1;
    this.sugerencia.fecha = new Date();
    this.sugerencia.usuario = this.id_usuario;
    this.sugerencia.id_pedido = this.id_pedido;
    this.sugerencia.id_sucursal = null;




    this.proveedorService.guardarSugerencia(this.sugerencia).then(data => {

      if (data == true) {
        let alert = this.alertCtrl.create({
          title: "Gracias por tu sugerencia",
          subTitle: "A pesar de que no podemos responder a todos tus mensajes personalmente, todas tus sugerencias son recibidas y valoradas con total profecionalidad.",
          buttons: [{
            text: 'OK',
            handler: () => {
              this.navCtrl.popToRoot();
            }
          }]
        });
        alert.present();
      }
    });

  }

  /*-----------------------------------------*/
  public doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.getListaPedidos(this.id_usuario);
      infiniteScroll.complete();
    }, 500);
  }
  /*-----------------------------------------*/
  // Cargar categorias
  public getListaPedidos(id_usuario) {

    let numPedidos = 5;
    this.pagina++;

    if (numPedidos * (this.pagina - 1) < this.totalPedidos)
      this.proveedorService.getListaPedidos(id_usuario, this.pagina, numPedidos)
        .then(data => {
          let arrayData: any;
          arrayData = data;

          for (var i = 0; i < arrayData.length; i++)
            this.listaPedidos.push(arrayData[i]);
          //arrayData[i]['ruta_foto']=sucursal.id_proveedor.id_perfil.ruta_foto;
          //this.listaPedidos.reverse();
          this.numPedidos = this.listaPedidos.length;
        });


  }
}
