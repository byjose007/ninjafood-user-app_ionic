import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { PlatoService } from '../../providers/plato-service/plato-service';
import { CarritoPage } from '../carrito/carrito';
import { PlatoModel } from '../../providers/models/plato-model';
import { MenuPage } from '../menu/menu';
import { ProveedorService } from '../../providers/proveedor-service/proveedor-service';
import {Constants} from '../../constantes/constants';
/*
  Generated class for the PaltoDetallePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/plato-detalle/plato-detalle.html',
  providers: [PlatoService, ProveedorService, Constants],//,
  //directives: [CarritoPage] 
})
export class PlatoDetallePage {
  plato: any;
  id_plato: any;
  sucursal: any;
  carrito: any[];
  cantidad: number = 1;
  lista_productos: any[] = [];
  platoModel: PlatoModel;
  id_tamanio: string;
  tamanios: any[] = [];

  id_menu: any;
  id_categoria: string;
  id_usuario:any;
  SERVER_IP: string;

  constructor(private navCtrl: NavController, private NavParams: NavParams, private PlatoService: PlatoService,
    private viewCtrl: ViewController, private ProveedorService: ProveedorService, public constants: Constants) {
    this.id_plato = this.NavParams.get('id_plato');
    this.id_menu = this.NavParams.get('id_menu');
    this.id_categoria = this.NavParams.get('id_categoria');
    this.sucursal = this.NavParams.get('sucursal');
    this.id_usuario = this.NavParams.get('id_usuario');
    if (this.NavParams.get('lista_productos') != undefined)
      this.lista_productos = this.NavParams.get('lista_productos');
    this.SERVER_IP = constants.SERVER_IP;
    this.loadPlato();

    //this.lista_productos = [];
  }

  private loadPlato() {
    this.plato = this.PlatoService.getPlato(this.id_plato)
      .then(plato => {
        plato = this.updateMainPath(plato);
        this.plato = plato;
        for (let tamanio in this.plato.tamanio) {
          if (this.plato.tamanio[tamanio].activo) {

            this.tamanios.push(plato.tamanio[tamanio]);
          }
        }
        this.id_tamanio = this.tamanios[0].id_tamanio.id_tamanio;
      });
  }

  updateMainPath(data) {
        var aux;
        if (data != null && typeof data !== undefined){
            aux = data.ruta_foto.replace('http://127.0.0.1:8080', this.SERVER_IP);
            data.ruta_foto = aux;
        }
        return data;
    }


  // carrito
 public actualizarCantidad(operacion) {
    if (operacion > 0)
      this.cantidad++;
    else {
      if (this.cantidad > 1)
        this.cantidad--;
    }
  }

 public agregarCarrito() {
    this.platoModel = new PlatoModel(this.plato, this.id_tamanio, this.cantidad);
    let existe = this.lista_productos.filter(p => p.id == this.platoModel.id && p.tamanioNombre == this.platoModel.tamanioNombre);
    this.ProveedorService.getTamanioPLato(this.platoModel.id, this.platoModel.id_tamanio)
      .then(tamanio => {
        let tam:any = tamanio;
       this.platoModel.id_tamanio = tam.id_tamanioplato;
        if (existe.length == 0)
          this.lista_productos.push(this.platoModel); 
        else
          for (let p in this.lista_productos){
            if (this.lista_productos[p].id == this.platoModel.id )
              this.lista_productos[p].cantidad += this.cantidad;
          };
               
      });    


    this.viewCtrl.dismiss(this.lista_productos);


  }

  public verCarrito() {

    this.navCtrl.push(CarritoPage, {
      id_usuario:this.id_usuario,
      lista_productos: this.lista_productos,
      sucursal: this.sucursal
    });

  }
}
