import { Component } from '@angular/core';
import { Validators, ControlGroup, FormBuilder } from '@angular/common';
import { NavController, NavParams, ViewController, AlertController, ModalController } from 'ionic-angular';
//import { AuthspaServices } from '../../providers/authspa/authspa';
import { HomePage } from '../home/home';
import { PedidoPage } from '../pedidos/pedidos';
import { ProveedorService } from '../../providers/proveedor-service/proveedor-service';
//import { Constants } from '../../constantes/constants';

@Component({
  templateUrl: 'build/pages/sugerencia/sugerencia.html',
  providers: [ProveedorService]
})

export class SugerenciaPage {

  id_usuario: number;
  tipo: any;
  observacion: string;
  sugerencia: any = {};
  id_pedido: any;
  id_sucursal: any;

  //Fecha actual
 /* anio: number = new Date().getFullYear();
  mes: number = new Date().getMonth();
  dia: number = new Date().getDate();
  hora: number = new Date().getHours();
  minuto: number = new Date().getMinutes();
  segundo: number = new Date().getSeconds();
  fecha_actual: Date;*/

  constructor(public proveedorService: ProveedorService, private navCtrl: NavController,
    private formBuilder: FormBuilder, private navParams: NavParams, public modalCtrl: ModalController,
    public alertCtrl: AlertController, private viewController: ViewController) {
    this.id_usuario = this.navParams.get('id_usuario');
    this.tipo = 'Otro';
   
  }

/*------------Pedidos a reportar---------------*/
  public presentModalPedidos() {
    
    if(this.tipo == 'Pedido')
    {
       this.navCtrl.push(PedidoPage,{id_usuario:this.id_usuario, reportar:true,tipoSugerencia:this.tipo})

    }else{
       this.navCtrl.push(PedidoPage,{id_usuario:this.id_usuario, reportar:true, tipo_sugerencia:this.tipo})

    }
  
  }

  /*------------Enviar sugerencia---------------*/

  public enviarSugerencia() {
    this.sugerencia.observacion = this.observacion;
    this.sugerencia.estado = 1;
    this.sugerencia.fecha = new Date();;
    this.sugerencia.usuario = this.id_usuario;


    switch (this.tipo) {
      case 'error':
        this.sugerencia.id_pedido = null;
        this.sugerencia.id_sucursal = null;
        this.sugerencia.tipo = 'Aplicacion_web';
        break;
      case 'new_function':
        this.sugerencia.id_pedido = null;
        this.sugerencia.id_sucursal = null;
        this.sugerencia.tipo = 'nueva_funcionalidad';
        break;
      case 'Pedido':        
        this.sugerencia.id_pedido = this.id_pedido;
        this.sugerencia.id_sucursal = this.id_sucursal;
        this.sugerencia.tipo = 'Pedido';

        break;
      case 'Servicio_transportista':
        this.sugerencia.id_pedido = this.id_pedido;
        this.sugerencia.id_sucursal = this.id_sucursal;
        this.sugerencia.tipo = 'Servicio_transportista';
        break;
      case 'otros':
        this.sugerencia.id_pedido = null;
        this.sugerencia.id_sucursal = null;
        this.sugerencia.tipo = 'Otro';
        break;
      default:
        this.sugerencia.id_pedido = null;
        this.sugerencia.id_sucursal = null;
        this.sugerencia.tipo = null;

    }

    this.proveedorService.guardarSugerencia(this.sugerencia).then(data => {

      if (data == true) {
        let alert = this.alertCtrl.create({
          title: "Gracias por tu sugerencia",
          subTitle: "A pesar de que no podemos responder a todos tus mensajes personalmente, todas tus sugerencias son recibidas y valoradas con total profecionalidad.",
          buttons: [{
            text: 'OK',

            handler: () => {              
              this.navCtrl.pop();
            }
          }]
        });
        alert.present();      

      }


    });

  }
}