import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, Platform, ToastController } from 'ionic-angular';
import { LocalNotifications } from 'ionic-native';
import { TimerWrapper } from '@angular/core/src/facade/async';
import { ProveedorService } from '../../providers/proveedor-service/proveedor-service';
import { Observable } from "rxjs/Rx";
/*
  Generated class for the EstadoPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/estado/estado.html',
  providers: [ProveedorService]
})

export class EstadoPage {
  tiempo_espera: number = 5;
  segundo: number = 0;
  tiempo_sucursal: number = 0
  tiempo_trans: number = 0;
  minuto: number;
  hora:number;
  horas:string;
  minutos: string;
  segundos: string;

  estado: number = 1;
  tiempo: string;
  count: number;
  id_pedido: any;
  pedido: any;
  primero: boolean = false;

  confirmado: boolean = false;
  enviado: boolean = false;
  entregado: boolean = false; // cuando el pedido ha sido entregado
  cancelado: boolean = false;
  estoyfuera: boolean = false; // el trasnportista esta fuera del lugar

  notificacion1: boolean = false;// notificacion sin confirmar
  notificacion2: boolean = false;// notificacion restaurante 
  notificacion3: boolean = false;// notificacion trasnportista
  notificacion4: boolean = false;// notificacion si trasnportista no responde  

  color_time: string = "rgb(255, 186, 20)";
  threadCrono: any;
  threadEstado: any;
  txt_size:any = "40";
  padd_top:any = "2.8";
  constructor(private NavParams: NavParams, private navCtrl: NavController,
    private alertCtrl: AlertController, private proveedorService: ProveedorService,
    protected platform: Platform, public toastCtrl: ToastController) {



    this.platform = platform;
    this.pedido = this.NavParams.get('pedido');
    this.id_pedido = this.pedido.id_pedido;

    this.minuto = this.tiempo_espera;
    this.threadCrono = TimerWrapper.setInterval(() => {

      /*---------------ESTADO NO CONFIRMADO-----------------*/
      if (this.estado == 1) {
        if (this.segundo == 0 && this.minuto == 0 && !this.entregado && !this.notificacion1  && this.hora==0) {

          this.notificacion1 = true;
          let alert1 = this.alertCtrl.create({
            title: "Mensaje",
            subTitle: 'El restaurante no a confirmado tu pedido, puedes esperar o cancelar tu pedido',
            buttons: [{
              text: 'Esperar',
              handler: data => {
                this.minuto = this.tiempo_espera;
                this.segundo=0;
                this.notificacion1 = false;
              }
            },
            {
              text: 'Cancelar Pedido',
              handler: () => {
                console.log('pedido cancelado');
                this.proveedorService.setEstado(this.pedido, 6).subscribe(
                  data => {
                    return true;
                  },
                  error => {
                    console.error("Error saving food!");
                    return Observable.throw(error);
                  });
              }
            }]
          });
          alert1.present();
          console.log("problema en restaurante");
        }
      }


      /*---------------ESTADO CONFIRMADO-----------------*/
      if (this.estado == 2  && !this.entregado && !this.notificacion2) {
        this.notificacion2 = true;

        LocalNotifications.schedule({
          id: 1,
          title: "El restaurante a confirmado tu pedido.",
          //text: 'El restaurante a confirmado tu pedido',
          led: 'FF0000',
          sound: this.platform.is('android') ? 'file://sounds/capisci_a.mp3' : 'file://sounds/capisci_ios.m4r'
          //at: new Date(new Date().getTime() + 3600),                
          //data: { secret: key }
        });


        let alert = this.alertCtrl.create({
          title: "Mensaje",
          subTitle: 'El restaurante a confirmado tu pedido.',
          buttons: ['OK']
        });
        alert.present();

        let audio = new Audio();
        audio.src = "file://sounds/capisci_a.mp3";
        audio.load();
        audio.play();

      } else if (this.estado == 2 && this.segundo == 0 && this.minuto == 0 && !this.notificacion4) {
        this.notificacion4 = true;
        let alert1 = this.alertCtrl.create({
          title: "Mensaje",
          subTitle: 'Ha habido un inconveniente en la espera del transportista, por favor ten paciencia.',
          buttons: [{ text: 'Ok', }]
        });
        alert1.present();
      }

      /*---------------ESTADO PEDIDO ENVIADO-----------------*/
      if (this.estado == 4 && !this.enviado) {
        this.enviado = true;
        this.presentToast('Tu pedido esta en camino.');
        LocalNotifications.schedule({
          id: 2,
          title: "Tu pedido esta en camino.",
          text: 'Puede tardar aproximadamente ' + this.minuto + ' minutos.',
          led: 'FF0000',
          sound: this.platform.is('android') ? 'file://sounds/capisci_a.mp3' : 'file://sounds/capisci_ios.m4r'
          //at: new Date(new Date().getTime() + 3600),

          //data: { secret: key }
        });
      }

      if (this.estado == 4) {
        if (this.segundo == 0 && this.minuto == 0 && !this.entregado && !this.notificacion3) {

          this.notificacion3 = true;
          let alert1 = this.alertCtrl.create({
            title: "Mensaje",
            subTitle: 'El transportista ha tenido un inconveniente , por favor ten paciencia.',
            buttons: ['OK']
          });
          alert1.present();
          console.log("problema en transportista");

          //this.entregado = true;
        }
      }


      /*---------------ESTADO ESTOY FUERA-----------------*/
      if (this.estado == 9 && !this.estoyfuera) {
        this.estoyfuera = true;
        LocalNotifications.schedule({
          id: 3,
          title: "El transportista se encuentra afuera.",
          text: 'puedes salir a recoger tu pedido',
          led: 'FF0000',
          sound: this.platform.is('android') ? 'file://sounds/capisci_a.mp3' : 'file://sounds/capisci_ios.m4r'
          //at: new Date(new Date().getTime() + 3600),                
          //data: { secret: key }
        });
      }

      /*---------------ESTADO ENTREGADO-----------------*/


      if (this.estado == 5 && !this.entregado) {
        this.entregado = true;
        this.presentToast('Pedido entregado');
        LocalNotifications.schedule({
          id: 4,
          title: "Disfruta de tu pedido.",
          text: '¡Gracias por confiar en Good Appetit!',
          led: 'FF0000',
          sound: this.platform.is('android') ? 'file://sounds/capisci_a.mp3' : 'file://sounds/capisci_ios.m4r'
          //at: new Date(new Date().getTime() + 3600),

          //data: { secret: key }
        });
        this.cancelPeriodicIncrement(this.threadCrono);
        this.cancelPeriodicIncrement(this.threadEstado);
      }

      /*---------------ESTADO PEDIDO CANCELADO-----------------*/

      if (this.estado == 6 && !this.cancelado) {
        this.cancelado = true;        
        LocalNotifications.schedule({
          id: 5,
          title: "Pedido Cancelado",
          text: 'Lamentamos los inconvenientes que hayamos causado',
          led: 'FF0000',
          sound: this.platform.is('android') ? 'file://sounds/capisci_a.mp3' : 'file://sounds/capisci_ios.m4r'
          //at: new Date(new Date().getTime() + 3600),

          //data: { secret: key }
        });
        this.cancelPeriodicIncrement(this.threadCrono);
        this.cancelPeriodicIncrement(this.threadEstado);
      }


      /*---------------CRONOMETRO-----------------*/
      this.segundos = "" + this.segundo;
      this.minutos = "" + this.minuto;
      if(this.horas==undefined || this.hora==null){
      this.horas="0";
      this.hora=0;
      }else{
        this.horas=""+this.hora;
      }
    
      if(this.minuto>=60){
       this.hora=parseInt(""+this.minuto/60);
       this.horas=""+this.hora;
       this.minuto=this.minuto%60;
       this.minutos=""+this.minuto;
      }

      if (this.segundo == 0 && this.minuto>0) {
          this.segundo = 60;
      }
      
      if (this.segundo == 0 && this.minuto>=0&& this.hora>0) {
          this.segundo = 60;
      }

      if (this.minuto < 10) {
        this.minutos = "0" + this.minuto;
      }

      if (this.segundo < 10) {
        this.segundos = "0" + this.segundo;
      }

      if(this.minuto==0 && this.hora>0 && this.segundo==60){
         this.minuto=60;
         this.hora--;  
      }
      
      if (this.segundo == 60) {
        this.segundos = "00";
        this.minuto--;
        //Ajuster en la inicializacion del tiempo - by Milton
      }
     
       if(this.horas!=undefined && this.horas!=null && this.horas!="0"){
         this.txt_size = "33";
         this.padd_top = "3.4";
         this.tiempo=this.horas+":"+this.minutos+":"+this.segundos;
       }
       else{
         this.txt_size = "40";
         this.padd_top = "2.8";
         this.tiempo=this.minutos+":"+this.segundos;
       }
   
      //Ajuster en la inicializacion del tiempo - by Milton
      if (this.segundo > 0 && !this.entregado) {
        this.segundo--;
      }

      /*if(this.minuto==0 && this.hora>0){
         this.hora--;
         }*/
      
    }, 1000);


    /*---------------CONSULTA DE ESTADOS CADA 5 SEGUNDOS-----------------*/
    this.threadEstado = TimerWrapper.setInterval(() => {
      this.getEstado();
      this.estado = this.pedido.id_estado;

      if(this.estado==5)

      if (this.estado == 2 && !this.confirmado) {

        this.tiempo_sucursal = this.pedido.tiempo;
        this.minuto = this.tiempo_sucursal;
        //this.minuto = this.pedido.tiempo;
        this.confirmado = true;   
        this.color_time = "rgba(255,180,0,.8)";
       }

      if (this.estado == 3 && !this.primero) {
        //this.presentToast('Pedido confirmado');
        this.tiempo_trans = this.pedido.tiempo;
        if (this.minuto > 1) {
          this.minuto = this.tiempo_trans; //- (this.tiempo_sucursal - this.minuto );
        }

        this.primero = true;
        this.color_time = "rgba(2, 120, 174, .8)";
       }
       if (this.estado == 4) {
         this.color_time = "rgba(37,165,95,.9)";
       }
       if (this.estado == 5) {
         this.color_time = "rgba(37,165,95,.9)";
       }
       if (this.estado == 6) {
         this.color_time = "rgba(228, 2, 45, 0.8)";
       }
       if (this.estado == 7) {
         this.color_time = "rgba(228, 2, 45, 0.8)";
       }
       if (this.estado == 8) {
         this.color_time = "rgba(228, 2, 45, 0.8)";
       }
    }, 5000);

    this.initializeApp();
  }


  public getEstado() {

    this.proveedorService.getEstado(this.id_pedido)
      .then(pedido => {
        this.pedido = pedido;
       //return pedido;
      });
  }

  /*---------------PRESENTAR MENSAJES TOAST-----------------*/
  public presentToast(mensaje_txt) {
    let toast = this.toastCtrl.create({
      message: mensaje_txt,
      duration: 5000
    });
    toast.present();
  }

  /*---------------BOTON IR A INICIO-----------------*/
  public gotoHome() {
    this.navCtrl.popToRoot();
  }

  /*---------------NO PERMITIR REGRESAR ATRAS-----------------*/
  initializeApp() {
    this.platform.ready().then(() => {
      document.addEventListener('backbutton', () => {
      this.gotoHome();
    });
    
      document.addEventListener('backbutton', () => {
        console.log('Back button tapped :)');
        this.gotoHome();
      }, false);
    });
  }

  cancelPeriodicIncrement(intervalId:any): void {
    if (!!intervalId) {
      TimerWrapper.clearInterval(intervalId);
      intervalId = null;
    }
  };

}
