import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HTTP_PROVIDERS, URLSearchParams, Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { NavController, Platform } from 'ionic-angular';
import { CustomValidations } from '../../theme/CustomValidations';
import { Constants } from '../../constantes/constants';


/*
  Generated class for the ProveedorService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ProveedorService extends CustomValidations {
  data: any;
  SERVER_IP: string;
  categories: any;
  pedido: any = {};
  direccion: any = {};
  error: any;
  perfil: any;
  constructor(private http: Http, protected navCtrl: NavController, protected platform: Platform, constants: Constants) {
    super(navCtrl, platform);
    this.data = null;
    this.SERVER_IP = constants.SERVER_IP;
    this.categories = null;
  }
  /*----------------------------*/

  public getPerfil(id_usuario) {

    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/perfil/' + id_usuario)
        .map(res => res.json())
        .subscribe(data => {
          this.perfil = data;
          resolve(this.perfil);
        });
    });

  }

  /*----------------------------*/
  public getDirecciones(id_usuario) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/direccion/' + id_usuario)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  /*----------------------------*/
  public guardarPedido(registro: any, detalles: any) {
    let body = JSON.stringify(registro);
    console.log("Enviar al servidor datos");
    console.log(body);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    registro.estado = 1;
    return new Promise(resolve => {
      this.http.post(this.SERVER_IP + '/api/rest/pedidoMovil/', body, options)
        .map(res => res.json())
        .subscribe(pedido => {
          this.pedido = pedido;

          let detalle = {
            valor_oferta: "",
            valor: "",
            cantidad_solicitada: "",
            tamanio: "",
            id_pedido: "",
            id_plato: ""
          };
          let temporal = "";
          detalles.forEach(element => {
            detalle.valor_oferta = element.valor_oferta != undefined ? element.valor_oferta : undefined;
            detalle.valor = element.valor_oferta != undefined ? element.valor_oferta : element.precio;
            detalle.cantidad_solicitada = element.cantidad;
            detalle.tamanio = element.id_tamanio;
            detalle.id_pedido = this.pedido.id_pedido;
            detalle.id_plato = element.id;


            let bodyDet = JSON.stringify(detalle);
            let headersDet = new Headers({ 'Content-Type': 'application/json' });
            let optionsDet = new RequestOptions({ headers: headersDet });
            this.http.post(this.SERVER_IP + '/api/rest/detallepedido/', bodyDet, optionsDet)
              .map(res => res.json())
              .subscribe(dataDetalles => {
                let det = dataDetalles;
                //resolveDet(det);
              },
              error => {
                console.error("Error saving detalles!");
                return Observable.throw(error);
              });

          });


          resolve(this.pedido);
        },
        error => {
          console.error("Error saving pedido!");
          return Observable.throw(error);
        });
    });
  }

  /*----------------------------*/
  public guardarDireccion(registro: any) {
    let body = JSON.stringify(registro);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      return this.http.post(this.SERVER_IP + '/api/rest/direcciones/', body, options)
        .map(res => res.json())
        .subscribe(data => {
          this.direccion = data;
          resolve(this.direccion);
        },
        error => {
          console.error("Error saving dirección!");
          return Observable.throw(error);
        });
    });
  }

  /*------------Todas las menus----------------*/
  public getMenus() {

    this.data = null;
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/menu/')
        .map(res => res.json(), err => this.checkServerResponse(err))
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  /*------------Todas las sucursales----------------*/
  public getSucursales() {

    this.data = null;
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/sucursal/')
        .map(res => res.json(), err => this.checkServerResponse(err))
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  /*----------------------------*/

  public getSucursal(id_sucursal) {

    this.data = null;
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/sucursal/' + id_sucursal)
        .map(res => res.json(), err => this.checkServerResponse(err))
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }
  /*----------------------------*/

  public getTamanioPLato(id_plato, tamanio) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/tamanioPLatoDetalle/' + id_plato + "/" + tamanio)
        .map(res => res.json(), err => this.checkServerResponse(err))
        .subscribe(dataTam => {
          //this.data = data;
          resolve(dataTam[0]);
        });
    });
  }
  /*----------------------------*/
  public getCategorias() {
    if (this.categories) {
      // already loaded categories
      return Promise.resolve(this.categories);
    }
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/categoriasP/')
        .map(res => res.json())
        .subscribe(categories => {
          this.categories = categories;
          resolve(this.categories);
        });
    });
  }


  /*----------------------------*/


  public getEstado(id_pedido) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/pedidoAll/' + id_pedido)
        .map(res => res.json(), err => this.checkServerResponse(err))
        .subscribe(data => {
          //this.data = data;
          resolve(data);
        });
    });


  }

  /*----------------------------*/

  public setEstado(pedido: any, estado: number) {
    pedido.id_estado = estado; // Problema del proveedor   
    let body = JSON.stringify(pedido);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let repos = this.http.put(this.SERVER_IP + '/api/rest/pedidoPendiente/' + pedido.id_pedido + '/', body, options)
      .map((res: Response) => res.json());
    return repos;

  }


  /*----------------------------*/

  public getListaPedidos(id_usuario, pagina, tamanio) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/listaPedidos/' + id_usuario + '/' + pagina + '/' + tamanio)
        .map(res => res.json(), err => this.checkServerResponse(err))
        .subscribe(listaPedidos => {
          //this.data = data;
          resolve(listaPedidos);
        });
    });


  }

  /*----------------------------*/

  public getTotalPedidos(id_usuario) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/totalPedidos/' + id_usuario)
        .map(res => res.json(), err => this.checkServerResponse(err))
        .subscribe(totalPedidos => {

          resolve(totalPedidos);
        });
    });
  }

  /*--------------GUARDAR SUGERENCIA--------------*/

    public guardarSugerencia(sugerencia: any) {
    let body = JSON.stringify(sugerencia);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      return this.http.post(this.SERVER_IP + '/api/rest/sugerencias/', body, options)
        .map(res => res.json())
        .subscribe(data => {
          //this.direccion = data;
          resolve(true);
        },
        error => {
          console.error("Error guardando sugerencias!");
          return Observable.throw(error);
        });
    });
  }

}
