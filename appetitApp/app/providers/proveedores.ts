
import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import { Constants } from '../constantes/constants';

//import {Observable} from 'rxjs/Observable';
//import 'rxjs/add/operator/map';
@Injectable()
export class LocalesService {
  data: any;
  SERVER_IP: string;
  //path: string = 'http://172.17.113.28:9000/api/rest/sucursal/?format=json';
  path: string = this.SERVER_IP+'/api/rest/sucursal/?format=json';

  constructor(private http: Http, public constants: Constants) {
    this.data = null;
    this.SERVER_IP = constants.SERVER_IP;
  }

  getLocales() {
    /*if (this.data) {
      return Promise.resolve(this.data);
    }
    let datos= new Promise(resolve => {
      this.http.get(this.path)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data.results;          
          resolve(this.data);
          
        });
    });
    //alert(datos);
    return datos;*/
     let repos = this.http.get(this.path);        
     return repos;
  }

}

