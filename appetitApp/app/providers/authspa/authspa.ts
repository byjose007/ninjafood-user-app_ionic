import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { URLSearchParams, Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import { NavController, Platform } from 'ionic-angular';
import { CustomValidations } from '../../theme/CustomValidations';
import { Constants } from '../../constantes/constants';

/*
  Generated class for the Authspa provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthspaServices extends CustomValidations {

  data: any;
  SERVER_IP: string;
  categories: any;
  usuario: any;
  error: any;
  cedula: string;
  perfil: any;

  constructor(private http: Http,
    protected navCtrl: NavController,
    protected platform: Platform, constants: Constants) {
    super(navCtrl, platform);
    this.data = null;
    this.SERVER_IP = constants.SERVER_IP;
  }
  /*----------------------------*/
  public verificarUsuario(email: any, first_name: any, last_name: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/usuario_movil/' + email + '/' + first_name + '/' + last_name)
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          console.log(this.usuario);
          resolve(this.usuario);
        },
        error => {
          console.error(this.SERVER_IP + " Error buscando email!");
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }


  /*----------------------------*/

  public getPerfil(id_usuario) {

    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/perfil/' + id_usuario)
        .map(res => res.json())
        .subscribe(data => {
          this.perfil = data;
          resolve(this.perfil);
        });
    });
  }

  /*----------------------------*/



  public actualizarUsuario(email: any, nombre: any, apellido: any, cedula: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/actualizar_usuario_movil/' + email + '/' + nombre + '/' + apellido + '/' + cedula)
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error buscando perfil!");
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }

  public actualizarNumeroTelefono(idPerfil: any, telefono: any) {
    return new Promise(resolve => {
      console.log("Datos a enviar en 'actualizar numero' "+idPerfil+" tel:"+telefono+"");
      this.http.get(this.SERVER_IP + '/api/rest/borrar_numero_movil/' + idPerfil + '/' + telefono )
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error actualizar numero perfil!");
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }

  public comprobarNumeroTelefono(idPerfil: any, telefono: any) {
    console.log("idPerfil: "+idPerfil);
    console.log("telefono: "+telefono);
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/comprobar_numero_movil/' + idPerfil + '/' + telefono )
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error buscando numero!");
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }

  public sendCodeEmailMovil(correo: any, code: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/sendCodeEmailMovil/' + correo + '/' + code )
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error buscando numero!");
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }

  public loginUsuario(usuario: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/loginUsuarioMovil/' + usuario.email + '/' + usuario.pass)
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error al logear usuario, intente mas tarde!");
          console.error("Error: "+error);
          this.checkServerResponse(error);
          resolve(Observable.throw(error));
          //return Observable.throw(error);
        });
    });
  }

  public registrarUsuario(usuario: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/registrarUsuarioMovil/' +
        /*usuario.usuario + '/' +*/ usuario.nombres + '/' + usuario.apellidos + '/' +
        usuario.cedula + '/' + usuario.email + '/' + usuario.password)
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error al registrar usuario, intente mas tarde!");
          console.error("Error: "+error);
          this.checkServerResponse(error);
          //return Observable.throw(error);
          resolve(Observable.throw(error));
        });
    });
  }

  public comprobarUsuario(usuario: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/comprobarUsuarioMovil/' + usuario.usuario)
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error al comprobar usuario, intente mas tarde!");
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }

  public comprobarCedula(usuario: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/comprobarCedulaMovil/' + usuario.cedula)
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error al comprobar cedula, intente mas tarde! :"+error);
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }

  public comprobarEmail(usuario: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/comprobarEmailMovil/' + usuario.email)
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error al comprobar email, intente mas tarde! :"+error);
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }

  public recuperarPass(email: any) {
    //let body = JSON.stringify(email);
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      return this.http.get(this.SERVER_IP + '/api/rest/recuperarPassMovil/'+email)
        .map(res => res.json())
        .subscribe(data => {
          //this.direccion = data;
          resolve(true);
        },
        error => {
          console.error("Error en email!");
          return Observable.throw(error);
        })
    });
  }

  public comprobarEmailPsw(email: any) {
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/comprobarEmailMovil/' + email)
        .map(res => res.json())
        .subscribe(data => {
          this.usuario = data;
          resolve(this.usuario);
        },
        error => {
          console.error("Error al comprobar email, intente mas tarde!");
          this.checkServerResponse(error);
          return Observable.throw(error);
        });
    });
  }

}



