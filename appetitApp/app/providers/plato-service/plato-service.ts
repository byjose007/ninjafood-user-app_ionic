import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Constants} from '../../constantes/constants';

/*
  Generated class for the PlatoService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PlatoService {
  data: any;
  plato: any;
  SERVER_IP: string;

  constructor(private http: Http, constants: Constants) {
    this.data = null;
    this.SERVER_IP = constants.SERVER_IP;
  }


  // Obtener platos de una determinda categoria
  getPlatos(id_menu, categoria) {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {

      this.http.get(this.SERVER_IP + '/api/rest/plato/m/' + id_menu + '/c/' + categoria + '/')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }


  // obtener datos de un solo plato 
  getPlato(id_plato) {

    //this.plato = this.data.filter(x=>x.id_plato = id_plato)[0];
    if (this.plato) {
      // already loaded data
      return Promise.resolve(this.plato);
    }
    return new Promise(resolve => {

      this.http.get(this.SERVER_IP + '/api/rest/plato/' + id_plato + '/')
        .map(res => res.json())
        .subscribe(plato => {
          this.plato = plato;
          resolve(this.plato);
        });
    });


  }



}

