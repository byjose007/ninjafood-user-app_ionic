import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Constants } from '../../constantes/constants';

/*
  Generated class for the CategoriaService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CategoriaService {
  data: any;
  SERVER_IP: string;

  constructor(private http: Http, constants: Constants) {
    this.data = null;
    this.SERVER_IP = constants.SERVER_IP;
  }


  getCategoriasSucursal(id_menu) {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      this.http.get(this.SERVER_IP + '/api/rest/listaPlatosCategoria/m/' + id_menu + '/')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }
}

