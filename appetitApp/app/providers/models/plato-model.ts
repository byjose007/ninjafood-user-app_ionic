
export class PlatoModel {
  id: string;
  nombre: string;
  tamanioNombre: string;
  cantidad: number;
  precio: number;
  id_tamanio: number
  valor_oferta: number;
  constructor(plato: any, id_tamanio: any, cantidad: number) {
    var listaTamanios = plato.tamanio;
    this.tamanioNombre = (listaTamanios.filter(p => p.id_tamanio.id_tamanio == id_tamanio)[0]).id_tamanio.tamanio;
    this.id_tamanio = (listaTamanios.filter(p => p.id_tamanio.id_tamanio == id_tamanio)[0]).id_tamanio.id_tamanio;
    this.nombre = plato.nombre_producto;
    this.id = plato.id_plato;
    this.cantidad = cantidad;
    this.valor_oferta = (listaTamanios.filter(p => p.id_tamanio.id_tamanio == id_tamanio)[0]).valor_oferta;

    if (this.valor_oferta != undefined && (listaTamanios.filter(p => p.id_tamanio.id_tamanio == id_tamanio)[0]).valor_oferta) {
      this.precio = (listaTamanios.filter(p => p.id_tamanio.id_tamanio == id_tamanio)[0]).valor_oferta * cantidad;
    } else {
      this.precio = (listaTamanios.filter(p => p.id_tamanio.id_tamanio == id_tamanio)[0]).valor * cantidad;
    }

  }
}