import {Component, ViewChild} from '@angular/core';
import {TabsPage} from './pages/tabs/tabs';
import { ionicBootstrap, Platform, NavController, Storage, LocalStorage, App, Nav } from 'ionic-angular';
import { StatusBar, SQLite } from 'ionic-native';
import { LoginPage } from './pages/login/login';
import { SlidesPage } from './pages/slides/slides';
import { CarritoPage } from './pages/carrito/carrito';
import { HomePage } from './pages/home/home';
import {Constants} from './constantes/constants';


@Component({
  // templateUrl: 'build/app.html',
  template: '<ion-nav [root]="rootPage"></ion-nav>',
  providers: [Constants]  
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  private rootPage: any;
  private rs: any;
  private usuario:any;
  private usuarioP:any;

  private testUser:any;

  constructor(private platform: Platform) {
    
    this.startPage().then(res => {
      console.log("####### APP.TS #######");
      console.log("res!!! " + res);
      this.rs = res;
      if(!res){
        this.rootPage = SlidesPage;
      } else {
        if (this.rs.registro == '0')
          this.rootPage = LoginPage;
        else {
          console.log("####### HOME #######: "+this.rs);
          this.getUserLogueado();
          this.setPage(HomePage);
        }       
      }
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  startPage(){
    return new Promise(resolve => {
      let db = new SQLite();
      let sqlQuerySesion = 'SELECT * FROM startApp';
      //let sqlQuerySesion = 'SELECT count(*) FROM startApp';  
      db.openDatabase({
        name: 'goodappetit.db',
        location: 'default' // the location field is required
      }).then(rta => {  
          db.executeSql(sqlQuerySesion, []).then(rta=>{
            console.log("rta!!! Login1: " + rta.rows.length);
            if(rta.rows.length!=0){
              console.log("fuck yea!!! Login");
              resolve(rta.rows.item(0));
            }else{
              resolve(rta.rows.item(0));
            }
          }, (err)=>{
            console.log("fuck yea!!! SLIDER");
            console.error(JSON.stringify(err));
            resolve(false);
          })
        }, (err) => {
          console.error(JSON.stringify(err));
          console.log("fuck yea!!! SLIDER"+err);
          resolve(false);
        });
        return(false);
    });
  }

  public getUserLogueado(){
    if(this.rs != null){
      this.usuario = {
        id_usuario: this.rs.id_usuario,
        id_perfil: this.rs.id_perfil,
        email: this.rs.email,
        nombre: this.rs.first_name,
        apellido: this.rs.last_name,
        foto: this.rs.foto,
        redsocial: this.rs.redsocial,
        cedula: this.rs.cedula
      };
    }
}

  public setPage(component: any): void {
    console.log("this.usuarioJSON "+JSON.stringify(this.usuario));
    this.nav.setRoot(component, { usuario: JSON.stringify(this.usuario) });
  }

}

//DEVELOP
//ionicBootstrap(MyApp);

//PRODUCTION
ionicBootstrap(MyApp, [],{
  prodMode: true
});
